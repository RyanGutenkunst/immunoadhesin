from __future__ import division

"""
Improved version of the Weibull distribtion.

myWeibull has a more accurate survival function and inverse survival function,
plus a meanAbove method.
"""
import numpy
import scipy.special
import scipy.stats.distributions as ssd 

def plica(x,y):
    return scipy.special.gamma(x) * scipy.special.gammaincc(x,y)

class myLogNorm(ssd.lognorm_gen):
    """
    blank
    """
    def _sf(self, x, s):
        return 0.5*scipy.special.erfc(numpy.log(x)/s/numpy.sqrt(2))

    def meanAbove(self, x, s, loc=0, scale=1):
        """
        Mean of values above some limit x.
        """
        integral = 0.5 * numpy.exp(0.5*s**2) * scale * (1 + scipy.special.erf((s**2 + numpy.log(scale) - numpy.log(x - loc))/(numpy.sqrt(2) * s)))
        return integral / self.sf(x, s,loc=loc,scale=scale) + loc

    # Need to override freeze so it includes meanAbove
    def freeze(self, s, loc=0, scale=1):
        frozen = ssd.rv_frozen(self, s, loc=loc, scale=scale)
        frozen.meanAbove = lambda x: self.meanAbove(x,s,loc,scale)
        return frozen

myLogNorm = myLogNorm(a=0, name='myLogNorm', shapes='s')

class myUniform(ssd.uniform_gen):
    """
    blank
    """
    def _sf(self, x):
        return max(0,min(1-x, 1))

    def _pdf(self, x):
        if x < 0:
            return 0
        elif x > 1:
            return 0
        else:
            return 1.

    def meanAbove(self, x, loc=0, scale=1):
        """
        Mean of values above some limit x.
        """
        if x > (loc + scale):
            raise ValueError('meanAbove is not defined for x larger than support of distribution.')
        if x < (loc + scale):
            return loc + scale/2
        return x + (loc + scale - x)/2

    # Need to override freeze so it includes meanAbove
    def freeze(self, loc=0, scale=1):
        frozen = ssd.rv_frozen(self, loc=loc, scale=scale)
        frozen.meanAbove = lambda x: self.meanAbove(x,loc,scale)
        return frozen

myUniform = myUniform(name='myUniform', shapes='')

class myWeibull_gen(ssd.frechet_r_gen):
    """
    blank
    """
    # Based off of scipy code for weibull_min. Annoying that I can't just
    # inherit from ssd.weibull_min
    def _sf(self, x, c):
        """
        Survival function (1 - cumulative distribution function)
        """
        return numpy.exp(-x**c)

    def _isf(self, x, c):
        """
        Inverse survival function
        """
        return (-numpy.log(x))**(1./c)

    def meanAbove(self, x, c, loc=0,scale=1):
        """
        Mean of values above some limit x.
        """
        if x < loc:
            return self.stats(c, loc=loc, scale=scale, moments='m')
        # Mathematica gave me the loc=0 and scale=1 cases. For the other I
        # do the integral numerically.
        if loc == 0:
            # Integral was evaluated in Mathematica and uses the "plica"
            # function.
            integral = scale*plica(1+1./c, (x/scale)**c)
        elif scale == 1:
            # Integral was evaluated in Mathematica and uses the "plica"
            # function.
            integral = numpy.exp(-(x-loc)**c)*x + plica(1./c, (x-loc)**c)/c
        else:
            if hasattr(self, 'cache'):
                if (x,c,loc,scale) in self.cache:
                    return self.cache[(x,c,loc,scale)]
            #def integrand(x,c,loc,scale):
            #    return x*self.pdf(x,c,loc=loc,scale=scale)
            #integral = scipy.integrate.quad(integrand, x, scipy.inf, 
            #                                args=(c,loc,scale))[0]
            #return integral / self.sf(x, c,loc,scale)

            # Avoid *many* numerical errors by scaling integral.
            def integrand_scaled(x,c):
                return x*self.pdf(x,c)
            integral = scipy.integrate.quad(integrand_scaled, (x-loc)/scale, 
                                            scipy.inf, args=(c,))[0]
            result = scale*integral / self.sf(x, c,loc,scale) + loc
            if hasattr(self, 'cache'):
                self.cache[(x,c,loc,scale)] = result
            return result

        return integral / self.sf(x, c,loc,scale)

    # Need to override freeze so it includes meanAbove
    def freeze(self, c, loc=0, scale=1):
        frozen = ssd.rv_frozen(self, c, loc=loc, scale=scale)
        frozen.meanAbove = lambda x: self.meanAbove(x,c,loc,scale)
        frozen.cache = {}
        return frozen

myWeibull = myWeibull_gen(a=0, name='myWeibull', shapes='c')

class fromData:
    def __init__(self, xx, yy):
        self.xx = xx

        dx = numpy.diff(xx)
        self.areas = (yy[:-1] + yy[1:])/2 * dx

        self.cumulative = numpy.empty(len(yy))
        self.cumulative[0] = 0
        self.cumulative[1:] = numpy.cumsum(self.areas)

        self.slopes = (yy[1:] - yy[:-1])/dx
        self.slopes[numpy.isnan(self.slopes)] = 0
        self.xareas = dx*(xx[:-1]*(2*yy[:-1] + yy[1:])
                          + xx[1:]*(yy[:-1] + 2*yy[1:]))/6
        self.xareas = (yy[:-1]*xx[:-1] + yy[1:]*xx[1:])/2 * dx

        norm = self.cumulative[-1]
        self.yy = yy/norm
        self.areas /= norm
        self.cumulative /= norm
        self.xareas /= norm

    def pdf(self, x):
        x = numpy.atleast_1d(x)
        # Value is between ii-1 and ii
        xx,yy = self.xx,self.yy
        ii = numpy.minimum(xx.searchsorted(x), len(xx)-1)
        p = yy[ii-1] + (yy[ii] - yy[ii-1])/(xx[ii] - xx[ii-1]) * (x - xx[ii-1])
        p[numpy.logical_or(x < xx[0], x > xx[-1])] = 0
        return p

    def cdf(self, x):
        x = numpy.atleast_1d(x)
        # Value is between ii-1 and ii
        xx,yy = self.xx,self.cumulative

        ii = numpy.minimum(xx.searchsorted(x), len(xx) - 1)
        
        c = yy[ii-1] + (yy[ii] - yy[ii-1])/(xx[ii] - xx[ii-1]) * (x - xx[ii-1])
        c[x <= xx[0]] = 0
        c[x >= xx[-1]] = 1
        return numpy.squeeze(c)

    def icdf(self, c):
        c = numpy.atleast_1d(c)
        xx,yy = self.xx,self.cumulative

        ii = numpy.minimum(yy.searchsorted(c), len(xx) - 1)

        x = (c - yy[ii-1]) * (xx[ii] - xx[ii-1])/(yy[ii] - yy[ii-1]) + xx[ii-1]
        x[c == 0] = xx[0]
        x[c == 1] = xx[-1]
        return x

    def sf(self, x):
        return 1-self.cdf(x)

    def isf(self, c):
        return self.icdf(1-c)

    def meanAbove(self, x):
        x = numpy.atleast_1d(x)
        xx,yy = self.xx, self.yy
        # ii will be the next point *past* this one
        ii = numpy.minimum(xx.searchsorted(x), len(xx) - 1)

        # We need to include points halfway along...
        y = self.pdf(x)
        numerator = (xx[ii] - x) * (x*y + yy[ii]*xx[ii])/2.
        numerator = self.xareas[ii:].sum()
        denominator = self.sf(x)

        result = numpy.atleast_1d(numerator/denominator)
        
        # It's not at all clear what's reasonable to do when requested value
        # is off-scale, or even when it's larger than largest seen value.
        result[numpy.atleast_1d(denominator) == 0] = xx[-1]
        result[x > xx[-1]] = xx[-1]

        return result
