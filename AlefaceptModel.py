import numpy
import scipy.optimize
import rootfuncs

delta_max = 0.5

#
# Immobile fraction fixed in contact region
#

def area_root_immobile_fin((e,r,delta,i,iin), L, ET, (beta,Kb10,Kb20), 
                           fixed_params, fin, alpha):
    Acell, rT, KR, KE, Kx, sigmaR, sigmaE = fixed_params

    # Assume equality of corresponding immobile and mobile parameters
    KI = KE
    KHE = Kx
    Kb01 = Kb10
    KFH = Kb20

    eT = ET/Acell

    ein = sigmaE * e
    rin = sigmaR * r

    e1 = 2 * KE * L * e
    e2 = Kx/2 * e1 * e
    e1in = 2 * KE * L * ein
    e2in = Kx/2 * e1in * ein

    b10 = Kb10 * e1in * rin
    b20 = Kb20 * e2in * rin

    r1 = KR * L * r
    r1in = KR * L * rin

    i1 = 2*KI * L * i
    i1in = 2*KI * L * iin

    h = KHE * e1 * i
    hin = KHE * e1in * iin

    b01 = Kb01 * i1in * rin
    b11 = KFH * hin * rin

    fout = 0.13

    totalein = ein + e1in + 2*e2in + iin + i1in + 2*hin + b01 + b10 + 2*b11 + 2*b20
    totaleout = e + e1 + 2*e2 + i + i1 + 2*h
    totalein = eT
    totaleout = eT

    resid = [
            (1-delta)*(e+e1+2*e2+h) + delta*(b10 + 2*b20 + b11 + hin + ein + e1in + 2*e2in) - ((1-fin)*delta*totalein + (1-fout)*(1-delta)*totaleout),
            iin + i1in + b01 + b11 + hin - fin*totalein,
            i + i1 + h - fout*totaleout,
            (r+r1)*(1-alpha*delta) + alpha*delta*(b10 + b20 + b01 + b11 + rin + r1in) - rT,
            b10 + b20 + b01 + b11 - beta
            ]
    return resid

def area_adhered_immobile_fin(L, ET, (beta,Kb10,Kb20), fixed_params, 
                              fin, alpha=0, icin=None,
                              return_raw=False):
    if alpha != 0:
        raise ValueError('alpha must be zero.')

    Acell = fixed_params[0]
    f_asym = f_asymptote(L, ET, (beta,Kb10,Kb20), fixed_params, alpha)
    if f_asym < fin:
        if return_raw:
            return [1,1,delta_max,1,1]
        else:
            return Acell*delta_max


    # I've had trouble getting good initial conditions for this fitting.
    # It seems to help to solve the all mobile case first.
    e0,r0,delta0 = area_adhered_all_mobile(L, ET, (beta,Kb10,Kb20), 
                                           fixed_params, alpha, 
                                           return_raw=True)

    Acell,rT,KR,KE,Kx,sigmaR,sigmaE = fixed_params
    KI = KE
    # This is the f = 1 solution for i0
    i0 = ET/Acell/(1 + 2*KI*L)

    if icin is None:
        ic = (e0,r0,delta0,i0,i0)
    else:
        ic = icin

    args=(L, ET, (beta,Kb10,Kb20), fixed_params, fin, alpha)
    (e,r,delta,i,iin), infodict,ier,mesg = \
            scipy.optimize.fsolve(area_root_immobile_fin, ic, args=args, 
                                  full_output=True)
    if ier != 1:
        # This seems to fail most often when the raw delta solution is < 0.
        ic = list(ic)
        ic[2] = -ic[2]
        (e,r,delta,i,iin), infodict,ier,mesg = \
                scipy.optimize.fsolve(area_root_immobile_fin, ic, 
                                      args=args, full_output=True)
    if ier != 1:
        ic[2] = 0
        (e,r,delta,i,iin), infodict,ier,mesg = \
                scipy.optimize.fsolve(area_root_immobile_fin, ic, 
                                      args=args, full_output=True)
    if return_raw:
        return e,r,delta,i,iin
    else:
        return Acell*max(min(delta,delta_max),0)

def ET_limit_immobile_fin(L, (beta,Kb10,Kb20), fixed_params, fin, alpha):
    """
    Minimum epitope count (not density) on cell for adhesion.

    L: Ligand concentration in uM
    f: Immobile fraction
    fixed_params: Acell rT, KR, KE, Kx, sigmaR, sigmaE
    """
    def root_func(ET):
        return area_adhered_immobile_fin(L, ET, (beta,Kb10,Kb20), fixed_params, 
                                         fin, alpha, return_raw=True)[2]
       
    return scipy.optimize.brentq(root_func,1e-6,5e5)

def integrand_fin(ET, L, (beta,Kb10,Kb20), fixed_params, fin, pdf, alpha):
    area = area_adhered_immobile_fin(L, ET, (beta,Kb10,Kb20), fixed_params, 
                                     fin, alpha)
    weight = pdf.pdf(ET)
    return area*weight

def avg_area_adhered_quad_immobile_fin(L, (beta,Kb10,Kb20), fixed_params, fin, 
                                   ETdist, eps, alpha=0, use_log=False):
    """
    Average area adhered
    """
    # Define our integration limits.
    # First, what is minimum epitope count for adhesion?
    Emin = ET_limit_immobile_fin(L, (beta,Kb10,Kb20), fixed_params, fin, alpha)
    # Then, integrate over the vast majority of the distribution
    Emax = ETdist.isf(1e-64)
    # Normalization factor for integration over distribution
    norm = ETdist.sf(Emin) - ETdist.sf(Emax)
    args = (L, (beta,Kb10,Kb20), fixed_params, fin, ETdist, alpha)
    # Do the integration
    if use_log:
        int = scipy.integrate.quad(integrand_log_fin, 
                                   numpy.log(Emin), numpy.log(Emax), 
                                   epsabs=eps, epsrel=eps, args=args)
    else:
        int = scipy.integrate.quad(integrand_fin, Emin, Emax, 
                                   epsabs=eps, epsrel=eps, args=args)
    return int[0]/norm

def fraction_adhered_immobile_fin(L, (beta,Kb10,Kb20), fixed_params, fin, 
                                  ETdist, alpha=0):
    """
    Fraction of cells adhered using a given distribution of ET.
    """
    Emin = ET_limit_immobile_fin(L, (beta,Kb10,Kb20), fixed_params, fin, alpha)
    return ETdist.sf(Emin)

#
# Immobile fraction function of fraction receptors in cross-links.
#

def ffunc_cross_power(fraction_crosslinked, exponent, diff=0.25):
    fraction_crosslinked = numpy.maximum(fraction_crosslinked, 0)
    f = 0.13 + fraction_crosslinked**exponent * diff
    f = numpy.maximum(numpy.minimum(f, 1), 0)
    return f

def ffunc_crosslink(fraction_crosslinked, cross_slope, curvature=0):
    f = 0.13 + cross_slope * fraction_crosslinked + 0.5*curvature*fraction_crosslinked**2
    f = numpy.maximum(numpy.minimum(f, 1), 0)
    return f

ffunc_crosslink_used = ffunc_crosslink

def fraction_crosslinked((e,r,delta,i,iin), L, ET, (beta,Kb10,Kb20), 
                         fixed_params, alpha):
    Acell, rT, KR, KE, Kx, sigmaR, sigmaE = fixed_params

    # Assume equality of corresponding immobile and mobile parameters
    KI = KE
    KHE = Kx
    Kb01 = Kb10
    KFH = Kb20

    eT = ET/Acell

    ein = sigmaE * e
    rin = sigmaR * r

    e1 = 2 * KE * L * e
    e2 = Kx/2 * e1 * e
    e1in = 2 * KE * L * ein
    e2in = Kx/2 * e1in * ein

    b10 = Kb10 * e1in * rin
    b20 = Kb20 * e2in * rin

    r1 = KR * L * r
    r1in = KR * L * rin

    i1 = 2*KI * L * i
    i1in = 2*KI * L * iin

    h = KHE * e1 * i
    hin = KHE * e1in * iin

    b01 = Kb01 * i1in * rin
    b11 = KFH * hin * rin

    fraction_crosslinked = 2*(delta*(e2in + b20 + hin + b11) + (1-delta)*(e2 + h))/eT
    return fraction_crosslinked

def area_root_immobile_crosslinks((e,r,delta,i,iin), L, ET, (beta,Kb10,Kb20), 
                                  fixed_params, ffunc_args, alpha):
    Acell, rT, KR, KE, Kx, sigmaR, sigmaE = fixed_params

    # Assume equality of corresponding immobile and mobile parameters
    KI = KE
    KHE = Kx
    Kb01 = Kb10
    KFH = Kb20

    eT = ET/Acell

    ein = sigmaE * e
    rin = sigmaR * r

    e1 = 2 * KE * L * e
    e2 = Kx/2 * e1 * e
    e1in = 2 * KE * L * ein
    e2in = Kx/2 * e1in * ein

    b10 = Kb10 * e1in * rin
    b20 = Kb20 * e2in * rin

    r1 = KR * L * r
    r1in = KR * L * rin

    i1 = 2*KI * L * i
    i1in = 2*KI * L * iin

    h = KHE * e1 * i
    hin = KHE * e1in * iin

    b01 = Kb01 * i1in * rin
    b11 = KFH * hin * rin

    fraction_crosslinked = 2*(delta*(e2in + b20 + hin + b11) + (1-delta)*(e2 + h))/eT
    f = ffunc_crosslink_used(fraction_crosslinked, *ffunc_args)

    return [
            (1-delta)*(e+e1+2*e2+h) + delta*(b10 + 2*b20 + b11 + hin + ein + e1in + 2*e2in) - (1-f)*eT,
            iin + i1in + b01 + b11 + hin - f*eT,
            i + i1 + h - f*eT,
            (r+r1)*(1-alpha*delta) + alpha*delta*(b10 + b20 + b01 + b11 + rin + r1in) - rT,
            b10 + b20 + b01 + b11 - beta
            ]

def area_adhered_immobile_crosslinks(L, ET, (beta,Kb10,Kb20), fixed_params, 
                                     ffunc_args, alpha=0, icin=None,
                                     return_raw=False):
    if alpha != 0:
        raise ValueError('alpha must be zero.')

    Acell = fixed_params[0]
    f_asym = f_asymptote(L, ET, (beta,Kb10,Kb20), fixed_params, alpha)
    if f_asym < 0.13:
        if return_raw:
            return [1,1,delta_max,1,1]
        else:
            return Acell*delta_max


    # I've had trouble getting good initial conditions for this fitting.
    # It seems to help to solve the all mobile case first.
    e0,r0,delta0 = area_adhered_all_mobile(L, ET, (beta,Kb10,Kb20), 
                                           fixed_params, alpha, 
                                           return_raw=True)

    Acell,rT,KR,KE,Kx,sigmaR,sigmaE = fixed_params
    KI = KE
    # This is the f = 1 solution for i0
    i0 = ET/Acell/(1 + 2*KI*L)

    if icin is None:
        ic = (e0,r0,delta0,i0,i0)
    else:
        ic = icin

    args=(L, ET, (beta,Kb10,Kb20), fixed_params, ffunc_args, alpha)
    (e,r,delta,i,iin), infodict,ier,mesg = \
            scipy.optimize.fsolve(rootfuncs.area_root_immobile_crosslinks, ic, args=args, 
                                  full_output=True)
    if ier != 1:
        # This seems to fail most often when the raw delta solution is < 0.
        ic = list(ic)
        ic[2] = -ic[2]
        (e,r,delta,i,iin), infodict,ier,mesg = \
                scipy.optimize.fsolve(rootfuncs.area_root_immobile_crosslinks, ic, 
                                      args=args, full_output=True,)
    if ier != 1:
        ic[2] = 0
        (e,r,delta,i,iin), infodict,ier,mesg = \
                scipy.optimize.fsolve(rootfuncs.area_root_immobile_crosslinks, ic, 
                                      args=args, full_output=True)
        #raise ValueError('Root-finding failed (thrice) in crosslinks.',
        #                 L, ET, (beta,Kb10,Kb20), fixed_params, 
        #                 ffunc_args, alpha, icin, return_raw)

    if return_raw:
        return e,r,delta,i,iin

    fcross_result = fraction_crosslinked((e,r,delta,i,iin), L, ET, 
                                         (beta,Kb10,Kb20), fixed_params, alpha)
    f_result = ffunc_crosslink_used(fcross_result, *ffunc_args)
    f_asym = f_asymptote(L, ET, (beta,Kb10,Kb20), fixed_params, alpha)
    if f_result > f_asym:
        return Acell*delta_max
    else:
        return Acell*max(min(delta,delta_max),0)

def ET_limit_immobile_crosslinks(L, (beta,Kb10,Kb20), fixed_params, ffunc_args,
                                 alpha, maxval=1e12):
    """
    Minimum epitope count (not density) on cell for adhesion.

    L: Ligand concentration in uM
    f: Immobile fraction
    fixed_params: Acell rT, KR, KE, Kx, sigmaR, sigmaE
    """
    def root_func(ET):
        return area_adhered_immobile_crosslinks(L, ET, (beta,Kb10,Kb20), 
                                           fixed_params, ffunc_args,
                                           alpha, return_raw=True)[2]
       
    try:
        return scipy.optimize.brentq(root_func,1e-6,maxval)
    except ValueError:
        return None

def integrand_crosslinks(ET, L, (beta,Kb10,Kb20), fixed_params, ffunc_args, 
                         pdf, alpha):
    Acell = fixed_params[0]
    area = area_adhered_immobile_crosslinks(L, ET, (beta,Kb10,Kb20), 
                                            fixed_params, ffunc_args, alpha)
    weight = pdf.pdf(ET)
    return area*weight

def avg_area_adhered_quad_immobile_crosslinks(L, (beta,Kb10,Kb20), fixed_params,
                                              ffunc_args, ETdist, eps, 
                                              alpha=0):
    """
    Average area adhered
    """
    # Define our integration limits.
    # Then, integrate over the vast majority of the distribution
    Emax = ETdist.isf(1e-64)
    # First, what is minimum epitope count for adhesion?
    Emin = ET_limit_immobile_crosslinks(L, (beta,Kb10,Kb20), fixed_params, 
                                        ffunc_args, alpha, maxval=Emax)
    # This implies that less than 1e-32 of the cells are adhering!
    if Emin is None:
        return 0
    # Normalization factor for integration over distribution
    norm = ETdist.sf(Emin) - ETdist.sf(Emax)
    args = (L, (beta,Kb10,Kb20), fixed_params, ffunc_args, ETdist, alpha)
    # Do the integration
    intout = scipy.integrate.quad(integrand_crosslinks, Emin, Emax, 
                                  epsabs=eps, epsrel=eps, full_output=True,
                                  args=args, limit=50)
    if len(intout) > 3:
        y,abserr,infodict,msg = intout
        raise ValueError(L, (beta,Kb10,Kb20), fixed_params, 
                         ffunc_args, ETdist, eps, alpha, y, abserr, infodict,
                         msg)

    return intout[0]/norm

def fraction_adhered_immobile_crosslinks(L, (beta,Kb10,Kb20), fixed_params, 
                                         ffunc_args, ETdist, alpha=0):
    """
    Fraction of cells adhered using a given distribution of ET.
    """
    Emax = ETdist.isf(1e-64)
    Emin = ET_limit_immobile_crosslinks(L, (beta,Kb10,Kb20), fixed_params, 
                                        ffunc_args, alpha, maxval=Emax)
    if Emin is None:
        return 0
    return ETdist.sf(Emin)

#
# Immobile fraction function of delta
#

def ffunc_delta_power(delta, exponent, diff=0.25):
    delta = numpy.maximum(delta, 0)
    f = 0.13 + delta**exponent * diff
    f = numpy.maximum(numpy.minimum(f, 1), 0)
    return f
def ffunc_delta_power_min(exponent, diff=0.25):
    return 0.13
ffunc_delta_power.min = ffunc_delta_power_min

def linear_ffunc(delta, slope, curvature=0):
    """
    Implement f = linear_function(delta)
    """
    f0 = 0.13
    f = f0 + slope*delta + 0.5*curvature*delta**2
    f = numpy.maximum(numpy.minimum(f, 1), 0)
    return f
def linear_ffunc_min(slope, curvature=0):
    return 0.13
linear_ffunc.min = linear_ffunc_min

def area_root_immobile_ffunc((e,r,delta,i,iin), L, ET, (beta,Kb10,Kb20), 
                             fixed_params, ffunc_args, alpha, ffunc, G):
    """
    Root-finding function for f = ffunc(delta)
    """
    Acell = fixed_params[0]
    f = ffunc(delta,*ffunc_args)
    return rootfuncs.area_root_immobile((e,r,delta,i,iin), L, ET, 
                                        (beta,Kb10,Kb20), 
                                        fixed_params, f=f, alpha=alpha, G=G)

def area_adhered_immobile_ffunc(L, ET, (beta,Kb10,Kb20), fixed_params, 
                                ffunc_args, alpha=0, ic=None,
                                return_raw=False, ffunc = linear_ffunc,
                                G = 0):
    # I've had trouble getting good initial conditions for this fitting.
    # It seems to help to solve the all mobile case first.
    e0,r0,delta0 = area_adhered_all_mobile(L, ET, (beta,Kb10,Kb20), 
                                           fixed_params, alpha, 
                                           return_raw=True)

    Acell,rT,KR,KE,Kx,sigmaR,sigmaE = fixed_params
    KI = KE
    # This is the f = 1 solution for i0
    i0 = ET/Acell/(1 + 2*KI*L)
    iin = i0

    # Starting delta at 1e-3 ensures that I almost certainly get the solution
    # with lower delta, if there is a solution
    if ic is None:
        ic = (e0,r0,1e-3,i0,iin)

    f_asym = f_asymptote(L, ET, (beta,Kb10,Kb20), fixed_params, alpha)
    if f_asym < ffunc.min(*ffunc_args):
        if return_raw:
            return [1,1,delta_max,1,1]
        else:
            # Implies that root-finding failed. This almost always means
            # that we're in the delta = 1 regime.
            return Acell*delta_max

    if len(ffunc_args) == 1 and ffunc is linear_ffunc:
        args=(L, ET, (beta,Kb10,Kb20), fixed_params, ffunc_args[0], alpha, G)
        (e,r,delta,i,iin), infodict,ier,mesg = \
                scipy.optimize.fsolve(rootfuncs.area_root_linear_delta, ic, args=args,
                                      full_output=True)

    else:
        args=(L, ET, (beta,Kb10,Kb20), fixed_params, ffunc_args, alpha, ffunc,G)
        (e,r,delta,i,iin), infodict,ier,mesg = \
                scipy.optimize.fsolve(area_root_immobile_ffunc, ic, args=args, 
                                      full_output=True)

    # Root-finding failed.
    if ier != 1:
        # If f_asym is > 1, failure is probably numerical issues. And result
        # is probably close to what we'd want.
        if f_asym > 1:
            if return_raw:
                return [e,r,delta,i,iin]
            else:
                return Acell * delta
        else:
            if return_raw:
                return [1,1,delta_max,1,1]
            else:
                # Implies that root-finding failed. This almost always means
                # that we're in the delta = 1 regime.
                return Acell*delta_max
    f_result = ffunc(delta, *ffunc_args)
    if f_result > f_asym:
        if return_raw:
            return [e,r,delta_max,i,iin]
        else:
            return Acell*delta_max


    if return_raw:
        return e,r,delta,i,iin

    return Acell*max(min(delta,delta_max),0)

def ET_limit_immobile_ffunc(L, (beta,Kb10,Kb20), fixed_params, ffunc_args, 
                            alpha, ffunc, G=0, Emax=1e12):
    """
    Minimum epitope count (not density) on cell for adhesion.

    L: Ligand concentration in uM
    f: Immobile fraction
    fixed_params: Acell rT, KR, KE, Kx, sigmaR, sigmaE
    """
    def root_func(ET):
        return area_adhered_immobile_ffunc(L, ET, (beta,Kb10,Kb20), 
                                           fixed_params, ffunc_args, alpha, 
                                           return_raw=True, ffunc=ffunc, G=G)[2]
       
    return scipy.optimize.brentq(root_func,1e-6,Emax)

def integrand_ffunc(ET, L, (beta,Kb10,Kb20), fixed_params, ffunc_args, 
                    pdf, alpha, ffunc):
    Acell = fixed_params[0]
    area = area_adhered_immobile_ffunc(L, ET, (beta,Kb10,Kb20), fixed_params, 
                                       ffunc_args, alpha, ffunc=ffunc)
    weight = pdf.pdf(ET)
    return area*weight

def avg_area_adhered_quad_immobile_ffunc(L, (beta,Kb10,Kb20), fixed_params, 
                                         ffunc_args, ETdist, eps, alpha=0,
                                         ffunc=linear_ffunc):
    """
    Average area adhered
    """
    # Define our integration limits.
    # First, what is minimum epitope count for adhesion?
    Emin = ET_limit_immobile_ffunc(L, (beta,Kb10,Kb20), fixed_params, 
                                   ffunc_args, alpha, ffunc)
    # Then, integrate over the vast majority of the distribution
    Emax = ETdist.isf(1e-32)
    # Normalization factor for integration over distribution
    norm = ETdist.sf(Emin) - ETdist.sf(Emax)
    args = (L, (beta,Kb10,Kb20), fixed_params, ffunc_args, ETdist, alpha, ffunc)
    # Do the integration
    intout = scipy.integrate.quad(integrand_ffunc, Emin, Emax, 
                                  epsabs=eps, epsrel=eps, full_output=True,
                                  args=args, limit=50)
    if len(intout) > 3:
        y,abserr,infodict,msg = intout
        raise ValueError(L, (beta,Kb10,Kb20), fixed_params, 
                         ffunc_args, ETdist, eps, alpha, y, abserr, infodict,
                         msg)

    return intout[0]/norm

def fraction_adhered_immobile_ffunc(L, (beta,Kb10,Kb20), fixed_params, 
                                    ffunc_args, ETdist, alpha=0, 
                                    ffunc=linear_ffunc, G=0):
    """
    Fraction of cells adhered using a given distribution of ET.
    """
    Emin = ET_limit_immobile_ffunc(L, (beta,Kb10,Kb20), fixed_params, 
                                   ffunc_args, alpha, ffunc=ffunc, G=G)
    return ETdist.sf(Emin)

#
# Fixed fraction of receptors immobile
#

def f_asymptote(L, ET, (beta,Kb10,Kb20), fixed_params,alpha=0,G=0):
    """
    Value of immobile fraction f at which immobile fraction itself is enough
    for adhesion.
    """
    if alpha != 0:
        raise ValueError('f asymptote expression only good for alpha = 0.')
    Acell,rT,KR,KE,Kx,sigmaR,sigmaE = fixed_params
    KG = KR
    rin = rT*sigmaR/(1.+KR*L+KG*G)
    Kb01 = Kb10
    KI = KE
    eT = 1.*ET/Acell
    return beta*(1.+2*KI*L*(1.+Kb01*rin))/(2.*Kb01*KI*L*eT*rin)

def area_adhered_immobile(L, ET, f, (beta,Kb10,Kb20), fixed_params, alpha=0,
                          return_raw=False, return_really_raw=False, G=0):
    if f == 0 and return_raw == False:
        result = area_adhered_all_mobile(L, ET, (beta,Kb10,Kb20), 
                                         fixed_params, alpha=alpha, 
                                         return_raw=return_raw)
        if return_raw:
            result = numpy.concatenate((result, [0,0]))
        return result

    if alpha != 0:
        raise ValueError("Method has not been well-tested for alpha != 0. "
                         "In particular, we don't have the asymptote solution "
                         "for alpha != 0.")
    # For the immobile case, I don't have nice expressions for the minimum ET
    # or L for adhesion. So I can't filter on those to begin with

    # First we check whether the immobile fraction is enough to drive adhesion.
    # In that case, delta would go to infinity. 
    # This is not the correct calculation for alpha != 0.
    Acell,rT,KR,KE,Kx,sigmaR,sigmaE = fixed_params
    KI = KE
    Kb01 = Kb10
    eT = ET/Acell

    rin = rT*sigmaR/(1+KR*L)
    iin = f*eT/(1+2*KI*L*(1+Kb01*rin))
    b01 = 2*Kb01*KI*L*iin*rin
    if b01 >= beta and not return_really_raw:
        # In this case, entire cell will be adhered.
        if return_raw:
            # return_raw clause is needed to work with ET_limit_immobile
            return [delta_max]*5
        else:
            return Acell*delta_max
    
    # I've had trouble getting good initial conditions for this fitting.
    # It seems to help to solve the all mobile case first.
    e0,r0,delta0 = area_adhered_all_mobile(L, ET, (beta,Kb10,Kb20), 
                                           fixed_params, alpha=alpha, 
                                           return_raw=True)

    # This is the f = 1 solution for i0
    i0 = ET/Acell/(1 + 2*KI*L)
    ic = (e0,r0,delta0,i0,iin)

    args=(L, ET, (beta,Kb10,Kb20), fixed_params, f, alpha, G)
    (e,r,delta,i,iin), infodict,ier,mesg = \
            scipy.optimize.fsolve(rootfuncs.area_root_immobile, ic, args=args,
                                  full_output=True)
    # It seems that root-finding typically fails when delta0 has the wrong sign.
    # We can retry to fix almost all the errors.
    if ier != 1:
        ic = (e0,r0,-delta0,i0,iin)
        (e,r,delta,i,iin), infodict,ier,mesg = \
                scipy.optimize.fsolve(rootfuncs.area_root_immobile, ic, 
                                      args=args, full_output=True)

    if ier != 1:
        # Implies that root-finding failed.
        function_args = (L, ET, f, (beta,Kb10,Kb20), fixed_params, alpha, 
                         return_raw)
        opt_results = (e,r,delta,i,iin), infodict,ier,mesg 
        raise ValueError('Root-finding failed in area_adhered_immobile.', 
                         (function_args, ic, opt_results))

    if return_raw:
        return e,r,delta,i,iin

    if e >= 0 and r >= 0 and delta >= 0:
        return Acell*min(delta,delta_max)
    elif e < 0 and r >= 0 and delta < 0:
        # XXX: It appears that e < 0 and delta < 0 implies that we're in the
        # high f regime where we actaully have full adhesion.
        return Acell * delta_max
    elif e >= 0 and r >= 0 and delta < 0:
        # XXX: It appears that e > 0 delta < 0 implies that we're in the
        # high f regime where we actaully have full adhesion.
        return 0
    else:
        raise ValueError

def ET_limit_immobile(L, f, (beta,Kb10,Kb20), fixed_params, alpha, G=0):
    """
    Minimum epitope count (not density) on cell for adhesion.

    L: Ligand concentration in uM
    f: Immobile fraction
    fixed_params: Acell rT, KR, KE, Kx, sigmaR, sigmaE
    """
    def root_func(ET):
        return area_adhered_immobile(L, ET, f, (beta,Kb10,Kb20), fixed_params, 
                                     alpha, return_raw=True, G=G)[2]
       
    return scipy.optimize.brentq(root_func,1e-6,5e5)


def integrand(ET, L, f, (beta,Kb10,Kb20), fixed_params, pdf, alpha):
    area = area_adhered_immobile(L, ET, f, (beta,Kb10,Kb20), fixed_params, 
                                 alpha)
    weight = pdf.pdf(ET)
    return area*weight

def integrand_log(logET, L, f, (beta,Kb10,Kb20), fixed_params, pdf, alpha):
    ET = numpy.exp(logET)
    return integrand(ET, L, f, (beta,Kb10,Kb20), fixed_params, pdf, alpha)*ET

def avg_area_adhered_quad_immobile(L, f, (beta,Kb10,Kb20), fixed_params, ETdist,
                                   eps, alpha=0, use_log=False):
    """
    Average area adhered
    """
    # Define our integration limits.
    # First, what is minimum epitope count for adhesion?
    Emin = ET_limit_immobile(L, f, (beta,Kb10,Kb20), fixed_params, alpha)
    # Then, integrate over the vast majority of the distribution
    Emax = ETdist.isf(1e-64)
    # Normalization factor for integration over distribution
    norm = ETdist.sf(Emin) - ETdist.sf(Emax)
    args = (L, f, (beta,Kb10,Kb20), fixed_params, ETdist, alpha)
    # Do the integration
    if use_log:
        int = scipy.integrate.quad(integrand_log, 
                                   numpy.log(Emin), numpy.log(Emax), 
                                   epsabs=eps, epsrel=eps, args=args)
    else:
        int = scipy.integrate.quad(integrand, Emin, Emax, 
                                   epsabs=eps, epsrel=eps, args=args)
    return int[0]/norm

def fraction_adhered_immobile(L, f, (beta,Kb10,Kb20), fixed_params, ETdist, 
                              alpha=0):
    """
    Fraction of cells adhered using a given distribution of ET.
    """
    Emin = ET_limit_immobile(L, f, (beta,Kb10,Kb20), fixed_params, alpha)
    return ETdist.sf(Emin)

#
# All receptors mobile. This case is numerically much simpler.
#

def area_adhered_all_mobile(L, ET, (beta,Kb1,Kb2), fixed_params, alpha=0, 
                            ic=(1e3, 1e3, 0.4), return_raw=False):
    """
    Solve eqns 10,12,13 from Byron's maunscript for Acell*delta.

    L: Ligand concentration in uM
    ET: *Total* epitopes on cell
    fixed_params: Acell, rT, KR, KE, Kx, sigmaR, sigmaE

    ic: initial condition for root finding.
    return_raw: Return e,r,delta solution of area_root_func.
    """
    ET = numpy.squeeze(ET)

    if not return_raw:
        # We can short-circuit and return 0 if we're outside of L limits
        Lmin,Lmax = L_limits(ET, (beta,Kb1,Kb2), fixed_params)
        if not Lmin < L < Lmax:
            return 0

    Acell = fixed_params[0]
    args=(L, ET, beta,Kb1,Kb2, fixed_params, alpha)
    popt = scipy.optimize.fsolve(rootfuncs.area_root_func, ic, args=args)
    e,r,delta = popt

    if return_raw:
        return e,r,delta
    else:
        if not numpy.all(popt > 0):
            # Solutions without all e,r,delta positive should have been filtered
            # out by L_Limits check.
            raise ValueError('Unexected negative results in area_adhered: '
                             , e,r,delta)
        return Acell*min(delta,delta_max)

def fraction_adhered_all_mobile(L, (beta,Kb1,Kb2), fixed_params, ETdist, G=0):
    """
    Fraction of cells adhered using a given distribution of ET.
    """
    Emin = ET_limit(L, (beta,Kb1,Kb2), fixed_params, G=G)
    return ETdist.sf(Emin)

def L_limits(ET, (beta,Kb1,Kb2), fixed_params):
    """
    Use polynomial from appendix to find min/max L for adhesion.

    If there is no L for which adhesion is possible, return (inf,inf).

    ET: *Total* epitopes on cell.
    fixed_params: Acell, rT, KR, KE, Kx, sigmaR, sigmaE
    """
    Acell, rT, KR, KE, Kx, sigmaR, sigmaE = fixed_params

    Kb1p = sigmaR * sigmaE * Kb1
    Kb2p = sigmaR * sigmaE**2 * Kb2

    Kb20 = Kb2
    Kb10 = Kb1

    eT = ET/Acell

    a0 = -rT*beta*Kb2p
    a1 = 2*rT*Kb1p*KE * (2*beta + eT*rT*Kb2p) + 4 * beta**2 * KE*Kx\
            + eT**2 * rT**2 * Kb2p**2 * KE * Kx\
            - rT*beta*Kb2p * (KR + 4 * KE * (1 + Kx*eT))
    # Incorrect version from manuscript
    #a2 = -(4*KE * (2*eT * rT**2 * Kb1p**2 *KE + beta*KR) + 2 * beta**2 *KR*Kx)\
    #        + 4 * KE**2 * Kb1p*rT * (2*beta + eT*rT*Kb2p)\
    #        + beta*rT*Kb2p * (KE + KR + eT*KR*Kx)
    # Corrected
    a2 = -4*KE*(beta*(Kb2p*(KE + KR)*rT - Kb1p*(2*KE + KR)*rT - 2*KR*Kx*beta) +
                eT*rT*(2*Kb1p**2*KE*rT - Kb1p*Kb2p*KE*rT + Kb2p*KR*Kx*beta))
    a3 = 4*beta*KE*KR * (2*rT*Kb1p*KE - rT*Kb2p*KE + beta*KR*Kx)

    #Power = numpy.power
    #a0 = -(Kb20*rT*beta*Power(sigmaE,2)*sigmaR)

    #a1 = 4*KE*Kx*Power(beta,2) + 4*Kb10*KE*rT*beta*sigmaE*sigmaR - 4*Kb20*KE*rT*beta*Power(sigmaE,2)*sigmaR - Kb20*KR*rT*beta*Power(sigmaE,2)*sigmaR - 4*eT*Kb20*KE*Kx*rT*beta*Power(sigmaE,2)*sigmaR + 2*eT*Kb10*Kb20*KE*Power(rT,2)*Power(sigmaE,3)*Power(sigmaR,2) + Power(eT,2)*Power(Kb20,2)*KE*Kx*Power(rT,2)*Power(sigmaE,4)*Power(sigmaR,2)

    #a2 = 8*KE*KR*Kx*Power(beta,2) + 8*Kb10*Power(KE,2)*rT*beta*sigmaE*sigmaR + 4*Kb10*KE*KR*rT*beta*sigmaE*sigmaR - 4*Kb20*Power(KE,2)*rT*beta*Power(sigmaE,2)*sigmaR - 4*Kb20*KE*KR*rT*beta*Power(sigmaE,2)*sigmaR - 4*eT*Kb20*KE*KR*Kx*rT*beta*Power(sigmaE,2)*sigmaR - 8*eT*Power(Kb10,2)*Power(KE,2)*Power(rT,2)*Power(sigmaE,2)* Power(sigmaR,2) + 4*eT*Kb10*Kb20*Power(KE,2)*Power(rT,2)*Power(sigmaE,3)* Power(sigmaR,2)

    #a3 = 4*KE*Power(KR,2)*Kx*Power(beta,2) + 8*Kb10*Power(KE,2)*KR*rT*beta*sigmaE*sigmaR - 4*Kb20*Power(KE,2)*KR*rT*beta*Power(sigmaE,2)*sigmaR

    r1,r2,r3 = numpy.roots([a3, a2, a1, a0])

    if not numpy.all(numpy.isreal([r1,r2,r3])):
        # In this case no adhesion is possible.
        return numpy.inf, numpy.inf
    else:
        # Should get one negative and two positive roots.
        return sorted([r1,r2,r3])[1:]

def ET_limit(L, (beta,Kb1,Kb2), fixed_params, G=0):
    """
    Minimum epitope count (not density) on cell for adhesion.

    L: Ligand concentration in uM
    fixed_params: Acell, rT, KR, KE, Kx, sigmaR, sigmaE
    """
    # Derived by eliminating L from eqns 16,17
    Acell, rT, KR, KE, Kx, sigmaR, sigmaE = fixed_params

    Kb1p = sigmaR * sigmaE * Kb1
    Kb2p = sigmaR * sigmaE**2 * Kb2

    KG = KR

    c = (1 + KR*L + KG*G)*beta*(-(Kb2p*(1 + 2*KE*L)**2*rT) + 
     4*KE*L*(Kb1p*(rT + 2*KE*L*rT) + Kx*(1 + KR*L + KG*G)*beta))
    b = 2*KE*L*rT*(-4*Kb1p**2*KE*L*rT + 
     Kb1p*Kb2p*(1 + 2*KE*L)*rT - 
     2*Kb2p*Kx*(1 + KR*L + KG*G)*beta)
    a = Kb2p**2*KE*Kx*L*rT**2

    # Factor of Acell here converts from eT to ET.
    return Acell*(-b + numpy.sqrt(b**2 - 4*a*c))/(2*a)

def avg_area_adhered_all_mobile(L, (beta,Kb1,Kb2), fixed_params, ETdist):
    """
    Average area adhered calculated using eqn 27.
    """
    Emin = ET_limit(L, (beta,Kb1,Kb2), fixed_params)
    ETstar = ETdist.meanAbove(Emin)
    if not 0 < ETstar < 1e16:
        return fixed_params[0] # Acell
    return area_adhered_all_mobile(L, ETstar, (beta,Kb1,Kb2), fixed_params)

#
# These are basically deprecated for the all mobile case, because I can use
# section 4.2 results to solve without integration.
#
#def average_area_adhered_discrete(L, (beta,Kb1,Kb2), fixed_params, 
#                                  Weibull_params, xx):
#    """
#    Integrate area adhered over Weibull dist.
#    """
#    Emin = ET_limit(L, (beta,Kb1,Kb2), fixed_params)
#
#    areas = numpy.zeros(len(xx))
#    for ii,ET in enumerate(xx):
#        if ET > Emin:
#            areas[ii] = area_adhered(L, ET, (beta,Kb1,Kb2), fixed_params)
#    nu, eta, gamma = Weibull_params
#    weights = weibull.pdf(xx, nu, scale=eta, loc=gamma)
#    # Only want to count adhered cells in this average
#    weights[areas <= 0] = 0
#    avg_area = scipy.integrate.trapz(areas * weights, xx)
#    norm = scipy.integrate.trapz(weights, xx)
#    avg_area /= scipy.integrate.trapz(weights, xx)
#    return avg_area
#
#def ffunc_prime(delta,scale_factor,Acell):
#    f0 = 0.13
#    f_max = 0.38
#    delta_max = 10./Acell
#    f = f0 + delta/delta_max*scale_factor*(f_max - f0)
#    f = max(min(f, 1), 0)
#
#    if f == 1 or f == 0:
#        return 0
#    else:
#        return 1./delta_max*scale_factor*(f_max - f0)
#
#def area_root_immobile_func_nor_fprime((e,delta,i,iin), L, ET, (beta,Kb10,Kb20),
#                                       fixed_params, f=None, scale_factor=1, 
#                                       alpha=0):
#    Acell, rT, KR, KE, Kx, sigmaR, sigmaE = fixed_params
#
#    # Assume equality of corresponding immobile and mobile parameters
#    KI = KE
#    KHE = Kx
#    Kb01 = Kb10
#    KFH = Kb20
#    eT = ET/Acell
#
#    r = rT/(1+KR*L)
#
#    f = ffunc(delta,scale_factor,Acell)
#
#    fprime = numpy.zeros((4,4))
#
#    fprime[0,0] = (1 + 2*KE*L + 2*i*KE*KHE*L + 4*e*KE*Kx*L)*(1 - delta) +\
#            delta*(sigmaE + 2*KE*L*sigmaE + 2*iin*KE*KHE*L*sigmaE + 
#                   4*e*KE*Kx*L*sigmaE**2 + 2*Kb10*KE*L*r*sigmaE*sigmaR + 
#                   2*iin*KE*KFH*KHE*L*r*sigmaE*sigmaR + 
#                   4*e*Kb20*KE*Kx*L*r*sigmaE**2*sigmaR)
#
#    fprime[0,1] = -e - 2*e*KE*L - 2*e*i*KE*KHE*L -\
#            2*e**2*KE*Kx*L + e*sigmaE + 2*e*KE*L*sigmaE +\
#            2*e*iin*KE*KHE*L*sigmaE + 2*e**2*KE*Kx*L*sigmaE**2 +\
#            2*e*Kb10*KE*L*r*sigmaE*sigmaR +\
#            2*e*iin*KE*KFH*KHE*L*r*sigmaE*sigmaR +\
#            2*e**2*Kb20*KE*Kx*L*r*sigmaE**2*sigmaR +\
#            eT*ffunc_prime(delta,scale_factor,Acell)
#
#    fprime[0,2] = 2*e*KE*KHE*L*(1 - delta)
#
#    fprime[0,3] = delta*(2*e*KE*KHE*L*sigmaE + 2*e*KE*KFH*KHE*L*r*sigmaE*sigmaR)
#
#    fprime[1,0] = 2*iin*KE*KHE*L*sigmaE + 2*iin*KE*KFH*KHE*L*r*sigmaE*sigmaR
#
#    fprime[1,1] = -eT*ffunc_prime(delta,scale_factor,Acell)
#
#    fprime[1,2] = 0
#
#    fprime[1,3] = 1 + 2*KI*L + 2*e*KE*KHE*L*sigmaE + 2*Kb01*KI*L*r*sigmaR +\
#            2*e*KE*KFH*KHE*L*r*sigmaE*sigmaR
#
#    fprime[2,0] = 2*i*KE*KHE*L
#
#    fprime[2,1] = -eT*ffunc_prime(delta,scale_factor,Acell)
#
#    fprime[2,2] = 1 + 2*e*KE*KHE*L + 2*KI*L
#
#    fprime[2,3] = 0
#
#    fprime[3,0] = 2*Kb10*KE*L*r*sigmaE*sigmaR +\
#            2*iin*KE*KFH*KHE*L*r*sigmaE*sigmaR +\
#            2*e*Kb20*KE*Kx*L*r*sigmaE**2*sigmaR
#
#    fprime[3,1] = 0
#
#    fprime[3,2] = 0
#
#    fprime[3,3] = 2*Kb01*KI*L*r*sigmaR + 2*e*KE*KFH*KHE*L*r*sigmaE*sigmaR
#
#    return fprime
#
#def area_root_immobile((e,r,delta,i,iin), L, ET, (beta,Kb10,Kb20), 
#                       fixed_params, f=0, alpha=0):
#    Acell, rT, KR, KE, Kx, sigmaR, sigmaE = fixed_params
#
#    # Assume equality of corresponding immobile and mobile parameters
#    KI = KE
#    KHE = Kx
#    Kb01 = Kb10
#    KFH = Kb20
#
#    eT = ET/Acell
#
#    ein = sigmaE * e
#    rin = sigmaR * r
#
#    e1 = 2 * KE * L * e
#    e2 = Kx/2 * e1 * e
#    e1in = 2 * KE * L * ein
#    e2in = Kx/2 * e1in * ein
#
#    b10 = Kb10 * e1in * rin
#    b20 = Kb20 * e2in * rin
#
#    r1 = KR * L * r
#    r1in = KR * L * rin
#
#    i1 = 2*KI * L * i
#    i1in = 2*KI * L * iin
#
#    h = KHE * e1 * i
#    hin = KHE * e1in * iin
#
#    b01 = Kb01 * i1in * rin
#    b11 = KFH * hin * rin
#
#    return [
#            (1-delta)*(e+e1+2*e2+h) + delta*(b10 + 2*b20 + b11 + hin + ein + e1in + 2*e2in) - (1-f)*eT,
#            iin + i1in + b01 + b11 + hin - f*eT,
#            i + i1 + h - f*eT,
#            (r+r1)*(1-alpha*delta) + alpha*delta*(b10 + b20 + b01 + b11 + rin + r1in) - rT,
#            b10 + b20 + b01 + b11 - beta
#            ]
#
