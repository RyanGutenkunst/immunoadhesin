"""
Script for optimizing parameters
"""
import numpy
from numpy import array
import scipy

from myDistributions import myWeibull as weibull
import Offset_fitting as Offfit
from fixed_data import *

#
# all mobile
#

#Cost: 1.18982988491
p0 = popt = array([  2.03901180e+02,   6.16800329e-01,   4.10704112e+01,
                   5.21394971e-07,   1.53891463e+00,   5.74874396e+03])

# Perturb parameters away from starting point
p0 = p0 * 2**(0.1*numpy.random.uniform(-1,1,size=len(p0)))
# Do optimization. This step is slow!
popt,cov_x,infodict,mesg,ier\
        = scipy.optimize.leastsq(Offfit.opt_resid_weibull_all_mobile, p0,
                                 args=(all_fixed_params, all_data),
                                 full_output=True)
print mesg
# Calculate final cost
cost = Offfit.opt_func(popt, all_fixed_params, all_data,
                       Offfit.opt_resid_weibull_all_mobile, args=[])
print cost
print repr(popt)

# Plot result of fit
ETdist = weibull(popt[-2], scale=popt[-1])
Offfit.make_plot(popt[:-2], fixed_params_high_rT,
                 fixed_params_low_rT,
                 high_rT_data, low_rT_data, ETdist, 
                 fignum=19, figtitle=None, xpts=21, model='all_mobile')

##
## fixed immobile
##
#
##Cost: 1.19022066269
#p0 = popt = array([  2.03802576e+02,   6.22856771e-01,   4.25081250e+01,
#                   5.14763214e-07,   9.10782489e-08,   1.53469293e+00,
#                   5.72341311e+03])
#
##p0 = p0 * 2**(numpy.random.uniform(-1,1,size=len(p0)))
##popt,cov_x,infodict,mesg,ier = scipy.optimize.leastsq(Offfit.opt_resid_weibull_fixed_immobile, p0,
##                                  args=(all_fixed_params, all_data), full_output=True)
##print mesg
#cost = Offfit.opt_func(popt, all_fixed_params, all_data, 
#                       Offfit.opt_resid_weibull_fixed_immobile, args=[])
#print cost
#print repr(popt)

#ETdist = weibull(popt[-2], scale=popt[-1])
#Offfit.make_plot(popt[:-2], fixed_params_high_rT,
#                 fixed_params_low_rT,
#                 high_rT_data, low_rT_data, ETdist, 
#                 fignum=18, figtitle=None, xpts=21, model='fixed_immobile')

##
## eta function of delta
##
#
## Cost: 0.961190837619
#p0 = popt = array([  2.02625904e+02,   3.96055939e-01,   4.22406335e+00,
#                   1.28057169e-05,   2.29508733e+01,   1.18452544e+00,
#                   1.82548506e+04])
#
##p0 = p0 * 2**(numpy.random.uniform(-1,1,size=len(p0)))
##popt,cov_x,infodict,mesg,ier = scipy.optimize.leastsq(Offfit.opt_resid_weibull_etafunc, p0,
##                                  args=(all_fixed_params, all_data), full_output=True)
##print mesg
#cost = Offfit.opt_func(popt, all_fixed_params, all_data, 
#                       Offfit.opt_resid_weibull_etafunc, args=[])
#print cost
#print repr(popt)

#ETdist = weibull(popt[-2], scale=popt[-1])
#Offfit.make_plot(popt[:-2], fixed_params_high_rT,
#                 fixed_params_low_rT,
#                 high_rT_data, low_rT_data, ETdist, 
#                 fignum=19, figtitle=None, xpts=21, model='etafunc')

##
## eta function of xlinks
##
#
## Cost: 1.20287649028
#p0 = popt = array([  2.00537875e+02,   5.47292162e-01,   3.59424216e+01,
#         7.35685175e-11,   9.64947426e-08,   1.58386402e+00,
#         6.79703979e+03])
#
##p0 = p0 * 2**(numpy.random.uniform(-1,1,size=len(p0)))
##popt,cov_x,infodict,mesg,ier = scipy.optimize.leastsq(Offfit.opt_resid_weibull_xlinks, p0,
##                                  args=(all_fixed_params, all_data), full_output=True)
##print mesg
#cost = Offfit.opt_func(popt, all_fixed_params, all_data, 
#                       Offfit.opt_resid_weibull_xlinks, args=[])
#print cost
#print repr(popt)
#
##ETdist = weibull(popt[-2], scale=popt[-1])
##Offfit.make_plot(popt[:-2], fixed_params_high_rT,
##                 fixed_params_low_rT,
##                 high_rT_data, low_rT_data, ETdist, 
##                 fignum=20, figtitle=None, xpts=101, model='xlinks')
