**Code for Model of Immunoadhesin-Mediated Cell Adhesion**

This code implements much of the analysis from Gutenkunst et al. _PLoS_ONE_ 6:e19701 (2011).

To get started, run "python setup.py" in the source directory. This will build the C extension module necessary for quickly running the model.

Then try running "python make_offset_all_figures.py". This will generate figures with the data and model results from Fig 3B of the figure. (Commented out in that script is also code to generate most other figures from the paper.)

The functions to calculate the area adhered, etc. are in Model_fitoffset.py. See docstrings of those functions for details on which is which. To add new distributions of epitope densities, modify myDistributions.py. The key is that the immunoadhesin model code depends on a meanAbove function which is not standard.

For fitting, Offset_weibul shows an example, although these functions rely on the sorts of data we had available for the initial modeling.
