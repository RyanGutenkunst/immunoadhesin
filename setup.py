import sys
import setuptools
from numpy.distutils import core

if 'build_ext' not in sys.argv:
    sys.argv.extend(['build_ext', '--inplace'])

ext = core.Extension(name='rootfuncs',
                     sources=['rootfuncs.pyf', 'rootfuncs.c'])
core.setup(ext_modules=[ext])
