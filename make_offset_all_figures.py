import numpy, pylab, scipy
from numpy import array

from myDistributions import myWeibull as weibull
from myDistributions import fromData, myLogNorm, myUniform
import AlefaceptModel as AM
import Model_fitoffset as M
import Offset_fitting as Offfit

# Load fixed values
from fixed_data import *
from numpy import linspace

from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Arial']})

#
# Generate fig 3B simulations. (Semi-manually merged plots to get red and blue
# lines in paper.)
#

popt = popt_offset_all_mobile
ETdist = weibull(popt[-2], scale=popt[-1])
Offfit.make_plot(popt[:-2], fixed_params_high_rT,
                 fixed_params_low_rT,
                 high_rT_data, low_rT_data, ETdist, 
                 fignum=11, figtitle=None, xpts=101, model='all_mobile')

popt = popt_offset_delta
ETdist = weibull(popt[-2], scale=popt[-1])
Offfit.make_plot(popt[:-2], fixed_params_high_rT,
                 fixed_params_low_rT,
                 high_rT_data, low_rT_data, ETdist, 
                 fignum=12, figtitle=None, xpts=101, model='etafunc')

#fig = pylab.figure(107, figsize=(3.25,3.25), dpi=300)
#fig.clear()
#    
#xx = 10**numpy.linspace(-4,2,51)
#
#popt = popt_offset_all_mobile
#popt = [204.00087118190626, 1.7615772626335042, 29.898175996306541, 0, 0.56484475127782985, 3494.5095859356898]
#ETdist = myLogNorm(popt[-2],scale=popt[-1])
#Offfit.make_plot(popt[:-2], fixed_params_high_rT,
#                 fixed_params_low_rT,
#                 high_rT_data, low_rT_data, ETdist, 
#                 fignum=11, figtitle=None, xpts=101, model='all_mobile')

#am_fig = pylab.figure(11)
#
#ax = fig.add_subplot(3,2,1)
#xx,yy = am_fig.axes[0].lines[0].get_data()
#ax.semilogx(xx,yy,'ko', ms=4, zorder=10)
#xx,yy = am_fig.axes[0].lines[1].get_data()
#ax.semilogx(xx,yy,'-k', lw=1.5)
#
#ax = fig.add_subplot(3,2,2)
#xx,yy = am_fig.axes[1].lines[0].get_data()
#ax.semilogx(xx,yy,'wo', ms=4, mew=1, zorder=10)
#xx,yy = am_fig.axes[1].lines[1].get_data()
#ax.semilogx(xx,yy,'-k', lw=1.5)
#
#ax = fig.add_subplot(3,2,3)
#xx,yy = am_fig.axes[2].lines[0].get_data()
#ax.semilogx(xx,yy,'ko', ms=4, zorder=10)
#xx,yy = am_fig.axes[2].lines[1].get_data()
#ax.semilogx(xx,yy,'-k', lw=1.5)
#
#ax = fig.add_subplot(3,2,4)
#xx,yy = am_fig.axes[3].lines[0].get_data()
#ax.semilogx(xx,yy,'wo', ms=4, mew=1, zorder=10)
#xx,yy = am_fig.axes[3].lines[1].get_data()
#ax.semilogx(xx,yy,'-k', lw=1.5)
#
#ax = fig.add_subplot(3,2,5)
#xx,yy = am_fig.axes[4].lines[0].get_data()
#ax.semilogx(xx,yy,'ko', ms=4, zorder=10)
#xx,yy = am_fig.axes[4].lines[1].get_data()
#ax.semilogx(xx,yy,'-k', lw=1.5)
#
#ax = fig.add_subplot(3,2,6)
#xx,yy = am_fig.axes[5].lines[0].get_data()
#ax.semilogx(xx,yy,'wo', ms=4, mew=1, zorder=10)
#xx,yy = am_fig.axes[5].lines[1].get_data()
#ax.semilogx(xx,yy,'-k', lw=1.5)
#
#for ii,ax in enumerate(fig.axes):
#    if ii in [0,1]:
#        ax.set_ylim(0,20)
#        ax.set_yticks([0,5,10,15,20])
#    elif ii in [2,3]:
#        ax.set_ylim(0,4200)
#        ax.set_yticks([0,2000,4000])
#    elif ii in [4,5]:
#        ax.set_ylim(0,1)
#        ax.set_yticks([0,0.25,0.5,0.75,1.0])
#    if ii == 0:
#        ax.set_title(r'$r_T$ = 1200 $\mu$m$^{-2}$', fontsize=10)
#        ax.set_ylabel('avg. specific\narea ($\\mu$m$^2\\!$)', fontsize=8)
#    elif ii == 1:
#        ax.set_title(r'$r_T$ = 625 $\mu$m$^{-2}$', fontsize=10)
#    elif ii == 2:
#        ax.set_ylabel('avg. bonds', fontsize=8, labelpad=10)
#    elif ii == 4:
#        ax.set_ylabel('fraction adhered', fontsize=8)
#
#    ax.set_xticks([1e-4,1e-2,1e0,1e2])
#
#    for tl in ax.get_yticklabels() + ax.get_xticklabels():
#        tl.set_fontsize(8)
#    for tl in ax.yaxis.get_minorticklines() + ax.xaxis.get_minorticklines():
#        tl.set_visible(False)
#
#    if not ax.is_last_row():
#        for tl in ax.get_xticklabels():
#            tl.set_visible(False)
#    else:
#        ax.set_xlabel(r'[Alefacept ($\mu$M)]', fontsize=10)
#    if not ax.is_first_col():
#        for tl in ax.get_yticklabels():
#            tl.set_visible(False)
#
#    # Remove ticklines from tops of subfigures.
#    try:
#        ax.xaxis.get_ticklines()[5].set_visible(False)
#        ax.xaxis.get_ticklines()[3].set_visible(False)
#    except:
#        pass
#
#fig.subplots_adjust(left=0.21,bottom=0.17,top=0.90,wspace=0.26,hspace=0.21)
#pylab.show()
#fig.savefig('Paper/fit_lognorm.pdf')

##
## bonds_v_area plot
##
#
#beta = 410
#
#fig = pylab.figure(87,figsize=(1.8,1.3), dpi=200)
#fig.clear()
#ax = fig.add_subplot(1,1,1)
#xx,yy = am_fig.axes[6].lines[0].get_data()
#ax.plot(xx,yy,'ko', ms=4, zorder=10)
#xx,yy = am_fig.axes[6].lines[1].get_data()
#ax.plot(xx,yy,'wo', ms=4, zorder=10, mew=1)
#xx,yy = am_fig.axes[6].lines[2].get_data()
#ax.plot(xx,yy,'-k', lw=1.5)
#
#ax.axis(ymax=4200)
#ax.set_xlabel(r'avg. area ($\mu$m$^2$)', fontsize=10)
#ax.set_ylabel(r'avg. bonds', fontsize=10)
#ax.set_yticks(linspace(0,4000,3))
#ax.set_xticks(linspace(0,20,6))
#for tl in ax.get_yticklabels() + ax.get_xticklabels():
#    tl.set_fontsize(8)
##ax.annotate(r'$A_{off}$', xy=(A0, 1e-6),  xycoords='data',
##            xytext=(-20, 15), textcoords='offset points',
##            arrowprops=dict(arrowstyle="->"), fontsize=8,
##            )
#    
#fig.subplots_adjust(left=0.32,bottom=0.30)
#    
#pylab.show()
#fig.savefig('Paper/bonds_v_area.pdf')
#
#
#import scipy.optimize
#
##
## Adherent region plot
##
#
#def specific_fraction_adhered(L, (beta,Kb1,Kb2), fixed_params, shape,
#                              desired_fraction, G=0):
#    def root_func(scale):
#        ETdist = weibull(shape,scale=scale)
#        return AM.fraction_adhered_all_mobile(L, (beta,Kb1,Kb2), fixed_params, 
#                                              ETdist, G=G) - desired_fraction
#    opt_scale = scipy.optimize.brentq(root_func,1e-6,1e12)
#    mu = weibull(shape, scale=opt_scale).stats('m')
#    return opt_scale, mu
#
#def specific_fraction_adhered_ffunc(L, (beta,Kb1,Kb2), fixed_params, shape, 
#                                    ffunc_args, desired_fraction, G=0):
#    def root_func(scale):
#        ETdist = weibull(shape,scale=scale)
#        return AM.fraction_adhered_immobile_ffunc(L, (beta,Kb1,Kb2), fixed_params, ffunc_args, ETdist, G=G) - desired_fraction
#    opt_scale = scipy.optimize.brentq(root_func,1e-6,1e12)
#    mu = weibull(shape, scale=opt_scale).stats('m')
#    return opt_scale, mu
#
#Acell,rT,KR,KE,Kx,sigmaR,sigmaE = fixed_params_high_rT
#beta,Kb1,Kb2 = popt_offset_all_mobile[:3]
#shape = popt_offset_all_mobile[-2]
#desired_fraction = 0.50
#
#fig = pylab.figure(94, figsize=(2.5,1.8), dpi=200)
#
#fig.clear()
#ax = fig.add_subplot(1,1,1)
#
#L_list1 = 10**linspace(-6,4,51)
#scale_list1, mu_list1 = [], []
#for L in L_list1:
#    scale,mu = specific_fraction_adhered(L, (beta,Kb1,Kb2), (Acell,rT,KR,KE,Kx,sigmaR,sigmaE), shape, desired_fraction)
#    scale_list1.append(scale)
#    mu_list1.append(mu)
#
#beta,Kb1,Kb2 = popt_offset_delta[:3]
#shape = popt_offset_delta[-2]
#linear_delta_args = [popt_offset_delta[4]]
#Kb10 = Kb1
#Kb20 = Kb2
#fixed_params = (Acell,rT,KR,KE,Kx,sigmaR,sigmaE)
#ffunc_args = linear_delta_args
#
#L_list2 = 10**linspace(-6,4,51)
#L_worked2 = []
#scale_list2, mu_list2 = [], []
#for ii,L in enumerate(L_list2):
#    print L,ii
#    try:
#        scale,mu = specific_fraction_adhered_ffunc(L, (beta,Kb1,Kb2), (Acell,rT,KR,KE,Kx,sigmaR,sigmaE), shape, linear_delta_args, desired_fraction)
#        L_worked2.append(L)
#        scale_list2.append(scale)
#        mu_list2.append(mu)
#    except ValueError:
#        pass
#
#L_list3 = 10**linspace(-6,4,51)
#L_worked3 = []
#scale_list3, mu_list3 = [], []
#for ii,L in enumerate(L_list3):
#    print L,ii
#    try:
#        scale,mu = specific_fraction_adhered_ffunc(L, (beta,Kb1,Kb2), (Acell,rT,KR,KE/10,Kx/10,sigmaR,sigmaE), shape, linear_delta_args, desired_fraction)
#        L_worked3.append(L)
#        scale_list3.append(scale)
#        mu_list3.append(mu)
#    except ValueError:
#        pass
#
#L_list4 = 10**linspace(-6,4,51)
#L_worked4 = []
#scale_list4, mu_list4 = [], []
#for ii,L in enumerate(L_list4):
#    print L,ii
#    try:
#        scale,mu = specific_fraction_adhered_ffunc(L, (beta,Kb1,Kb2), (Acell,rT,KR,KE,Kx,sigmaR,sigmaE), shape, linear_delta_args, desired_fraction, G=9.0)
#        L_worked4.append(L)
#        scale_list4.append(scale)
#        mu_list4.append(mu)
#    except ValueError:
#        pass
#
#ax.loglog(mu_list1,L_list1,'-b', lw=1.5)
#ax.loglog(mu_list2,L_worked2,'-r', lw=1.5)
#ax.loglog(mu_list3,L_worked3,'-g', lw=1.5,zorder = 15)
## This is a fudge based on extrapolation...
#ax.loglog([mu_list2[0]*numpy.sqrt(10)] + mu_list4, [L_worked2[0]] + L_worked4,'-k', lw=1.5, zorder= 10)
#
#beta,Kb1,Kb2 = popt_offset_all_mobile[:3]
#eT = numpy.array([1,1e7])
#yy = eT/Acell * Kb1 * rT * sigmaE * sigmaR/(beta * KR)
#ax.loglog(eT,yy, '-k', lw=0.5)
#yy = beta/(KE*(eT/Acell)**2 * Kb2 * Kx * rT * sigmaE**2 * sigmaR)
#ax.loglog(eT,yy, '-k', lw=0.5)
#
#ax.set_yticks(10**linspace(-6,4,11))
#[l.set_visible(False) for l in ax.get_yticklabels()[1::2]]
#ax.set_ylabel(r'[Alefacept] ($\mu$M)', fontsize=10)
#ax.set_xticks(10**linspace(1,6,6))
#ax.set_xlabel('avg. CD2 per cell', fontsize=10)
#ax.axis(xmax=1e6, xmin=100, ymax=10**4, ymin=10**-6)
#fig.subplots_adjust(left=0.21,bottom=0.23)
#for tl in ax.get_yticklabels() + ax.get_xticklabels():
#    tl.set_fontsize(8)
#
#for tl in ax.yaxis.get_minorticklines() + ax.xaxis.get_minorticklines():
#    tl.set_visible(False)
#
#fig.show()
#
#fig.savefig('Paper/LvE.pdf')
#
#
##
## FACS data plot
##
#
#import scipy.integrate 
#
## Load facs data
#facs = numpy.loadtxt('facs.dat')
## Normalize it
#norm = scipy.integrate.trapz(facs[:,1], facs[:,0])
#
#fig = pylab.figure(99, figsize=(2.5,1.8), dpi=200)
#fig.clear()
#ax = fig.add_subplot(1,1,1)
#ax.semilogx(facs[:,0], facs[:,1], '-k', lw=0.5)
#ax.set_xlim(1e3,1e6)
#ax.set_yticks([])
#ax.set_xlabel('CD2 per cell', fontsize=10)
#fig.subplots_adjust(left=0.07, right=0.93, bottom=0.22, top=0.95)
#for tl in ax.get_yticklabels() + ax.get_xticklabels():
#    tl.set_fontsize(8)
#
#fig.savefig('Paper/flowcyt.pdf')

pylab.show()
