import random
import numpy

# Load the data
facs_data = numpy.loadtxt('facs.dat')
high_rT_data = numpy.loadtxt('high_rT.dat')
low_rT_data = numpy.loadtxt('low_rT.dat')

## Make datasets with A0 subtracted out.
#A0 = 6.173
#high_rT_data_noA0 = high_rT_data.copy()
#high_rT_data_noA0[:,3] = numpy.maximum(high_rT_data[:,3] - A0, 0)
#low_rT_data_noA0 = low_rT_data.copy()
#low_rT_data_noA0[:,3] = numpy.maximum(low_rT_data_noA0[:,3] - A0, 0)

# Fixed parameters
Acell = 800 # um^2
high_rT = 1.2e3 # um^-2
low_rT = 6.25e2 # um^-2
KR = 1.0 # uM^-1
KE = 6.7e-1 # uM^-1
Kx = 4.5e-2 # um^2
sigmaR = sigmaE = 0.75
fixed_params_high_rT = Acell, high_rT, KR, KE, Kx, sigmaR, sigmaE
fixed_params_low_rT = Acell, low_rT, KR, KE, Kx, sigmaR, sigmaE

all_fixed_params = [fixed_params_high_rT, fixed_params_low_rT]
all_data = [high_rT_data, low_rT_data]

#popt_all_mobile = array([4.12780684e+02,   1.31954344e+00,   7.61531900e+01,
#                         1.88170587e+00,   6.24534466e+03])
#popt_linear_delta = array([4.09813517e+02,   6.09521360e-01,   6.67934558e+00,
#                           1.47776954e+00,   2.22058738e+04,   43.0])

# for Kx = 4.5e-2
#Kx = 4.5e-2
#popt_all_mobile = array([  4.12786435e+02,   1.33367191e+00,   5.44938282e+01,
#                         1.88096661e+00,   6.23256725e+03])
#popt_linear_delta = array([  4.10579501e+02,   6.70605397e-01,   5.51097841e+00,
#                           1.48029199e+00,   2.06430475e+04,   4.15443674e+01])

popt_offset_all_mobile = numpy.array([  2.03901180e+02,   6.16800329e-01,   4.10704112e+01, 5.21394971e-07,   1.53891463e+00,   5.74874396e+03])
popt_offset_delta = numpy.array([  2.02625904e+02,   3.96055939e-01,   4.22406335e+00, 1.28057169e-05,   2.29508733e+01,   1.18452544e+00, 1.82548506e+04])

def make_bootstrap_data(high_in, low_in):
    high_pts, low_pts = len(high_in), len(low_in)
    total_pts = high_pts + low_pts

    # First, determine how many high and low rT points we'll need.
    prop_high, prop_low = 1.*high_pts/total_pts, 1.*low_pts/total_pts
    boot_high, boot_low = numpy.random.multinomial(total_pts, 
                                                   [prop_high, prop_low])

    high_new = [random.choice(high_in) for ii in xrange(boot_high)]
    low_new = [random.choice(low_in) for ii in xrange(boot_low)]

    return numpy.array(high_new), numpy.array(low_new)
