import sys
import numpy

import myDistributions
from myDistributions import myWeibull as weibull
from myDistributions import fromData, myLogNorm, myUniform

import AlefaceptModel as OM
import Model_fitoffset as M

int_eps = 1e-3

"""
Optimization functions for Heterogenous cells with omega offset
"""

def resid_given_ET(params,fixed_params_list,data_list, ET,
                   model="all_mobile", etafunc=OM.linear_ffunc,
                   area_scaling=1):
    """
    Residuals of data and model given a distribution of ET.
    """
    resid = []

    for fixed_params, data in zip(fixed_params_list, data_list):
        Acell = fixed_params[0]
        
        L_used = data[:,0]

        # Predictions of average area adhered
        if model == "all_mobile":
            beta,Kb1,Kb2,omega=params
            area_pred = [M.area_adhered_all_mobile(L, ET, (beta,Kb1,Kb2,omega), fixed_params) for L in L_used]
        elif model == 'fixed_immobile':
            beta,Kb1,Kb2,omega,f=params
            area_pred = [M.area_adhered_fixed_immobile(L, ET, f, (beta,Kb1,Kb2,omega), fixed_params) for L in L_used]
        elif model == 'etafunc':
            beta,Kb10,Kb20,omega = params[:4]
            etafunc_args = params[4:]
            area_pred = [M.area_adhered_immobile_delta_func(L, ET, (beta,Kb10,Kb20,omega), fixed_params, etafunc_args, ffunc=etafunc) for L in L_used]
        elif model == 'xlinks':
            beta,Kb10,Kb20,omega = params[:4]
            etafunc_args = params[4:]
            area_pred = [M.area_adhered_immobile_xlinks_func(L, ET, (beta,Kb10,Kb20,omega), fixed_params, etafunc_args, ffunc=etafunc) for L in L_used]

        area_pred = numpy.array(area_pred)

        # Invert our area v. bonds relation to find average bonds
        bonds_pred = (area_pred - omega*Acell)*beta

        area_data = data[:,3]
        bonds_data = data[:,2]
        frac_adhered_data = data[:,1]

        # We normalize each of our three data sets by the maximum observed.
        area_norm = max(area_data)
        bonds_norm = max(bonds_data)

        # We ignore bonds and area pseudomeasurements that correspond to 
        # data where no cells were observed to be adhered.
        good_data = frac_adhered_data != 0

        # Apply our scaling factor for the contact area, to get to the observed
        # contact area
        area_pred = area_pred/area_scaling

        resid.extend((area_pred[good_data] - area_data[good_data])/area_norm)
        resid.extend((bonds_pred[good_data] - bonds_data[good_data])/bonds_norm)

    resid = numpy.array(resid)
    print (resid**2).sum()
    sys.stdout.flush()
    return resid

def resid_given_dist(params,fixed_params_list,data_list, ETdist,
                     model='all_mobile', etafunc=OM.linear_ffunc):
    """
    Residuals of data and model given a distribution of ET.
    """
    resid = []

    for fixed_params, data in zip(fixed_params_list, data_list):
        Acell = fixed_params[0]
        
        L_used = data[:,0]

        # Predictions of average area adhered
        if model == 'all_mobile':
            beta,Kb1,Kb2,omega = params
            area_pred = [M.avg_area_adhered_all_mobile(L, (beta,Kb1,Kb2,omega), fixed_params, ETdist, eps=int_eps) for L in L_used]
        elif model == 'fixed_immobile':
            beta,Kb1,Kb2,omega,f = params
            area_pred = [M.avg_area_adhered_fixed_immobile(L, f, (beta,Kb1,Kb2,omega), fixed_params, ETdist, eps=int_eps) for L in L_used]
        elif model == "etafunc":
            beta,Kb10,Kb20,omega = params[:4]
            etafunc_args = params[4:]
            area_pred = [M.avg_area_adhered_immobile_delta_func(L, (beta,Kb10,Kb20,omega), fixed_params, ETdist, etafunc_args, ffunc=etafunc) for L in L_used]
        elif model == "xlinks":
            beta,Kb10,Kb20,omega = params[:4]
            etafunc_args = params[4:]
            area_pred = [M.avg_area_adhered_immobile_xlinks_func(L, (beta,Kb10,Kb20,omega), fixed_params, ETdist, etafunc_args, ffunc=etafunc) for L in L_used]

        area_pred = numpy.array(area_pred)

        # Invert our area v. bonds relation to find average bonds
        bonds_pred = (area_pred - omega*Acell)*beta

        # Now the fraction adhered.
        # Here we use omega=0 result...
        if model == 'all_mobile':
            beta,Kb1,Kb2,omega = params
            frac_pred = [OM.fraction_adhered_all_mobile(L, (beta,Kb1,Kb2), fixed_params, ETdist) for L in L_used]
        elif model == 'fixed_immobile':
            beta,Kb1,Kb2,omega,f = params
            frac_pred = [OM.fraction_adhered_immobile(L, f, (beta,Kb1,Kb2), fixed_params, ETdist) for L in L_used]
        elif model == "etafunc":
            beta,Kb10,Kb20,omega = params[:4]
            etafunc_args = params[4:]
            frac_pred = [OM.fraction_adhered_immobile_ffunc(L, (beta,Kb10,Kb20), fixed_params, etafunc_args, ETdist, ffunc=etafunc) for L in L_used]
        elif model == "xlinks":
            beta,Kb10,Kb20,omega = params[:4]
            etafunc_args = params[4:]
            frac_pred = [OM.fraction_adhered_immobile_crosslinks(L, (beta,Kb10,Kb20), fixed_params, etafunc_args, ETdist) for L in L_used]

        frac_pred = numpy.array(frac_pred)

        area_data = data[:,3]
        bonds_data = data[:,2]
        frac_adhered_data = data[:,1]

        # We normalize each of our three data sets by the maximum observed.
        area_norm = max(area_data)
        bonds_norm = max(bonds_data)
        frac_norm = max(frac_adhered_data)

        # We ignore bonds and area pseudomeasurements that correspond to 
        # data where no cells were observed to be adhered.
        good_data = frac_adhered_data != 0

        resid.extend((area_pred[good_data] - area_data[good_data])/area_norm)
        resid.extend((bonds_pred[good_data] - bonds_data[good_data])/bonds_norm)
        resid.extend((frac_pred - frac_adhered_data)/frac_norm)

    resid = numpy.array(resid)
    print (resid**2).sum()
    sys.stdout.flush()
    return resid

def opt_resid_weibull_all_mobile(params, fixed_params_list, 
                                 data_list):
    """
    Residual function for working with all_mobile model and weibull distribution
    of epitope densities.
    """
    print repr(params)
    sys.stdout.flush()
    # Basic model parameters
    beta,Kb1,Kb2,omega = params[:4]
    # Weibull parameters
    nu,eta = params[-2:]
    gamma = 0
    if numpy.any(params < 0):
        # Make a huge cost if parameters are nonsensible.
        # Note that 62 is the total length of the data.
        return numpy.array([100]*62)

    ETdist = weibull.freeze(nu,scale=eta,loc=gamma)

    return resid_given_dist((beta,Kb1,Kb2,omega), fixed_params_list, data_list, 
                            ETdist)

def opt_resid_lognorm_all_mobile(params, fixed_params_list, 
                                 data_list):
    print repr(params)
    sys.stdout.flush()
    beta,Kb1,Kb2,omega = params[:4]
    # LogNormal parameters
    shape,scale = params[-2:]
    if numpy.any(params < 0):
        return numpy.array([100]*62)

    ETdist = myLogNorm(shape,scale=scale)

    return resid_given_dist((beta,Kb1,Kb2,omega), fixed_params_list, data_list, 
                            ETdist)


facs_data = numpy.loadtxt('facs.dat')
def opt_resid_FACS_all_mobile(params, fixed_params_list, data_list):
    print repr(params)
    sys.stdout.flush()
    beta,Kb1,Kb2,omega = params[:4]
    if numpy.any(numpy.array(params) < 0):
        return numpy.array([100]*62)

    ETdist = fromData(facs_data[:,0], facs_data[:,1])

    return resid_given_dist((beta,Kb1,Kb2,omega), fixed_params_list, data_list, 
                            ETdist)

def opt_resid_uniform_all_mobile(params, fixed_params_list, 
                                 data_list):
    print repr(params)
    beta,Kb1,Kb2,omega = params[:4]
    # Weibull parameters
    min,width = params[-2:]
    if numpy.any(params < 0):
        return numpy.array([100]*62)

    ETdist = myUniform(loc=min,scale=width)

    return resid_given_dist((beta,Kb1,Kb2,omega), fixed_params_list, data_list, 
                            ETdist)

def opt_resid_weibull_fixed_immobile(params, fixed_params_list, 
                                     data_list):
    print repr(params)
    beta,Kb1,Kb2,omega,f = params[:5]
    nu,eta = params[-2:]
    gamma = 0
    if numpy.any(params < 0):
        return numpy.array([100]*62)

    ETdist = weibull.freeze(nu,scale=eta,loc=gamma)

    return resid_given_dist((beta,Kb1,Kb2,omega,f), fixed_params_list, 
                            data_list, ETdist, model='fixed_immobile')

def opt_resid_lognormal_fixed_immobile(params, fixed_params_list, 
                                     data_list):
    print repr(params)
    beta,Kb1,Kb2,omega,f = params[:5]
    shape,scale = params[-2:]
    gamma = 0
    if numpy.any(params < 0):
        return numpy.array([100]*62)

    ETdist = myLogNorm(shape,scale=scale)

    return resid_given_dist((beta,Kb1,Kb2,omega,f), fixed_params_list, 
                            data_list, ETdist, model='fixed_immobile')

def opt_resid_lognormal_all_mobile(params, fixed_params_list, 
                                   data_list):
    print repr(params)
    beta,Kb1,Kb2,omega = params[:4]

    shape,scale = params[-2:]
    if numpy.any(params < 0):
        return numpy.array([100]*62)

    ETdist = myLogNorm(shape,scale=scale)

    return resid_given_dist((beta,Kb1,Kb2,omega), fixed_params_list, data_list, 
                            ETdist)

def opt_resid_deltafunc_all_mobile(params, fixed_params_list, 
                                   data_list):
    """
    Residuals for Weibull distribution of ET (with gamma=0), plus fixed
    fraction f immobile receptors.
    """
    print repr(params)
    beta,Kb1,Kb2,omega = params[:4]
    ET = params[4]
    gamma = 0
    if numpy.any(params < 0):
        return numpy.array([100]*40)

    return resid_given_ET((beta,Kb1,Kb2,omega), fixed_params_list, data_list, 
                            ET)

def opt_resid_deltafunc_fixed_immobile(params, fixed_params_list, 
                                       data_list):
    """
    Residuals for Weibull distribution of ET (with gamma=0), plus fixed
    fraction f immobile receptors.
    """
    print repr(params)
    beta,Kb1,Kb2,omega,f = params[:5]
    ET = params[5]
    gamma = 0
    if numpy.any(params < 0):
        return numpy.array([100]*40)

    return resid_given_ET((beta,Kb1,Kb2,omega,f), fixed_params_list, data_list, 
                            ET, model='fixed_immobile')

def opt_resid_deltafunc_etafunc(params, fixed_params_list, 
                                data_list, etafunc=OM.linear_ffunc):
    print repr(params)
    if numpy.any(params < 0):
        return numpy.array([100]*40)

    return resid_given_ET(params[:-1], fixed_params_list, data_list, params[-1],
                          model='etafunc', etafunc=etafunc)

def opt_resid_deltafunc_xlinks(params, fixed_params_list, 
                               data_list, etafunc=M.ffunc_linear_xlink):
    print repr(params)
    if numpy.any(params < 0):
        return numpy.array([100]*40)

    return resid_given_ET(params[:-1], fixed_params_list, data_list, params[-1],
                          model='xlinks')

def opt_resid_weibull_etafunc(params, fixed_params_list, data_list,
                              etafunc=OM.linear_ffunc):
    print repr(params)
    if numpy.any(params < 0):
        return numpy.array([100]*62)

    nu,eta = params[-2:]
    ETdist = weibull.freeze(nu,scale=eta,loc=0)

    return resid_given_dist(params[:-2], fixed_params_list, 
                            data_list, ETdist, model='etafunc', etafunc=etafunc)

def opt_resid_weibull_xlinks(params, fixed_params_list, data_list,
                             etafunc=M.ffunc_linear_xlink):
    print repr(params)
    if numpy.any(params < 0):
        return numpy.array([100]*62)

    nu,eta = params[-2:]
    ETdist = weibull.freeze(nu,scale=eta,loc=0)

    return resid_given_dist(params[:-2], fixed_params_list, 
                            data_list, ETdist, model='xlinks', etafunc=etafunc)

def opt_resid_areascaling_all_mobile(params, fixed_params_list, data_list, 
                                     fixed_ET):
    print repr(params)
    if numpy.any(params < 0):
        return numpy.array([100]*40)
    #if params[-1] > 1:
    #    return numpy.array([100]*40)

    return resid_given_ET(params[:4], fixed_params_list, 
                          data_list, ET = fixed_ET, model='all_mobile', 
                          area_scaling=params[4])

def opt_func(params, fixed_params_list, data_list, resid_func, args):
    """
    Optimization function that simply sums over squared residuals for a given
    residual function.
    """
    resid = resid_func(params, fixed_params_list, data_list, *args)
    return sum(resid**2)

def make_plot(params, fixed_params_high_rT,fixed_params_low_rT,
              high_rT_data, low_rT_data, ETdist, 
              fignum=None, figtitle=None, xpts=31, model='all_mobile',
              etafunc=OM.linear_ffunc
              ):
    """
    Plot fit results, assuming distribution for epitope density across cells

    param: Fit parameters, which depend on the model employed.
    fixed_params_high_rT: Fixed parameters for high receptor density data
    fixed_params_low_rT: Fixed parameters for low receptor density data
    high_rT_data: High receptor density data
    low_rT_data: Low receptor density data
    ETdist: Epitope density distribution
    """
    import matplotlib.pyplot as pyplot

    Acell = fixed_params_high_rT[0]

    # Reproduce plot 1
    fig = pyplot.figure(fignum, figsize=(8,10))
    fig.clear()
    
    xx = 10**numpy.linspace(-4,2,xpts)
    
    if model == 'all_mobile':
        beta,Kb10,Kb20,omega = params
        yy_high_rT = numpy.array([M.avg_area_adhered_all_mobile(L, (beta,Kb10,Kb20,omega), fixed_params_high_rT, ETdist, eps=int_eps) for L in xx])
        yy_low_rT = numpy.array([M.avg_area_adhered_all_mobile(L, (beta,Kb10,Kb20,omega), fixed_params_low_rT, ETdist, eps=int_eps) for L in xx])
    elif model == 'fixed_immobile':
        beta,Kb10,Kb20,omega,f = params
        yy_high_rT = numpy.array([M.avg_area_adhered_fixed_immobile(L, f, (beta,Kb10,Kb20,omega), fixed_params_high_rT, ETdist, eps=int_eps) for L in xx])
        yy_low_rT = numpy.array([M.avg_area_adhered_fixed_immobile(L, f, (beta,Kb10,Kb20,omega), fixed_params_low_rT, ETdist, eps=int_eps) for L in xx])
    elif model == "etafunc":
        beta,Kb10,Kb20,omega = params[:4]
        etafunc_args = params[4:]
        yy_high_rT = [M.avg_area_adhered_immobile_delta_func(L, (beta,Kb10,Kb20,omega), fixed_params_high_rT, ETdist, etafunc_args, ffunc=etafunc) for L in xx]
        yy_low_rT = [M.avg_area_adhered_immobile_delta_func(L, (beta,Kb10,Kb20,omega), fixed_params_low_rT, ETdist, etafunc_args, ffunc=etafunc) for L in xx]
    elif model == "xlinks":
        beta,Kb10,Kb20,omega = params[:4]
        etafunc_args = params[4:]
        yy_high_rT = [M.avg_area_adhered_immobile_xlinks_func(L, (beta,Kb10,Kb20,omega), fixed_params_high_rT, ETdist, etafunc_args, ffunc=etafunc) for L in xx]
        yy_low_rT = [M.avg_area_adhered_immobile_xlinks_func(L, (beta,Kb10,Kb20,omega), fixed_params_low_rT, ETdist, etafunc_args, ffunc=etafunc) for L in xx]

    good_high_data = high_rT_data[:,1] != 0
    good_low_data = low_rT_data[:,1] != 0

    ax = fig.add_subplot(4,2,1)
    ax.semilogx(high_rT_data[:,0][good_high_data], high_rT_data[:,3][good_high_data], 'ko', ms=8)
    lims = ax.axis()
    ax.semilogx(xx, yy_high_rT, '-k', lw=2)
    ax.axis(lims)
    ax.axis(xmin=xx.min(), ymin=0, ymax=20, xmax=xx.max())
    ax.set_ylabel('Avg contact area', fontsize='x-large')
    ax.set_xlabel('[Alefacept ($\\mu$)]', fontsize='x-large')
    pyplot.xticks(fontsize='x-large')
    pyplot.yticks(fontsize='x-large')
    ax.set_title(r'$r_T$ = 1200/$\mu m^2$', fontsize='x-large')
    
    ax = fig.add_subplot(4,2,2)
    ax.semilogx(low_rT_data[:,0][good_low_data], low_rT_data[:,3][good_low_data], 'wo', ms=8, mew=2)
    lims = ax.axis()
    ax.semilogx(xx, yy_low_rT, '-k', lw=2)
    ax.axis(lims)
    ax.axis(xmin=xx.min(), ymin=0, ymax=20, xmax=xx.max())
    ax.set_ylabel('Avg contact area', fontsize='x-large')
    ax.set_xlabel('[Alefacept ($\\mu$)]', fontsize='x-large')
    pyplot.xticks(fontsize='x-large')
    pyplot.yticks(fontsize='x-large')
    ax.set_title(r'$r_T$ = 625/$\mu m^2$', fontsize='x-large')

    yy_high_rT = numpy.array(yy_high_rT)
    yy_low_rT = numpy.array(yy_low_rT)

    b_high_rT = (yy_high_rT - omega*Acell)*beta
    b_low_rT = (yy_low_rT - omega*Acell)*beta

    ax = fig.add_subplot(4,2,3)
    ax.semilogx(high_rT_data[:,0][good_high_data], high_rT_data[:,2][good_high_data], 'ko', ms=8)
    lims = ax.axis()
    ax.semilogx(xx, b_high_rT, '-k', lw=2)
    ax.axis(lims)
    ax.axis(ymin=0, ymax = 4200, xmin=xx.min(), xmax=xx.max())
    ax.set_ylabel('Avg bridging bonds', fontsize='x-large')
    ax.set_xlabel('[Alefacept ($\\mu$)]', fontsize='x-large')
    pyplot.yticks([0,1000,2000,3000,4000])
    pyplot.xticks(fontsize='x-large')
    pyplot.yticks(fontsize='x-large')
    
    ax = fig.add_subplot(4,2,4)
    ax.semilogx(low_rT_data[:,0][good_low_data], low_rT_data[:,2][good_low_data], 'wo', ms=8, mew=2)
    lims = ax.axis()
    ax.semilogx(xx, b_low_rT, '-k', lw=2)
    ax.axis(lims)
    ax.axis(ymin=0, ymax = 4200, xmin=xx.min(), xmax=xx.max())
    ax.set_ylabel('Avg bridging bonds', fontsize='x-large')
    ax.set_xlabel('[Alefacept ($\\mu$)]', fontsize='x-large')
    pyplot.yticks([0,1000,2000,3000,4000])
    pyplot.xticks(fontsize='x-large')
    pyplot.yticks(fontsize='x-large')
    
    if model == 'all_mobile':
        beta,Kb10,Kb20,omega = params
        fa_high_rT = [OM.fraction_adhered_all_mobile(L, (beta,Kb10,Kb20), fixed_params_high_rT, ETdist) for L in xx]
        fa_low_rT = [OM.fraction_adhered_all_mobile(L, (beta,Kb10,Kb20), fixed_params_low_rT, ETdist) for L in xx]
    elif model == 'fixed_immobile':
        beta,Kb10,Kb20,omega,f = params
        fa_high_rT = [OM.fraction_adhered_immobile(L, f, (beta,Kb10,Kb20), fixed_params_high_rT, ETdist) for L in xx]
        fa_low_rT = [OM.fraction_adhered_immobile(L, f, (beta,Kb10,Kb20), fixed_params_low_rT, ETdist) for L in xx]
    elif model == "etafunc":
        beta,Kb10,Kb20,omega = params[:4]
        etafunc_args = params[4:]
        fa_high_rT = [OM.fraction_adhered_immobile_ffunc(L, (beta,Kb10,Kb20), fixed_params_high_rT, etafunc_args, ETdist, ffunc=etafunc) for L in xx]
        fa_low_rT = [OM.fraction_adhered_immobile_ffunc(L, (beta,Kb10,Kb20), fixed_params_low_rT, etafunc_args, ETdist, ffunc=etafunc) for L in xx]
    elif model == "xlinks":
        beta,Kb10,Kb20,omega = params[:4]
        etafunc_args = params[4:]
        fa_high_rT = [OM.fraction_adhered_immobile_crosslinks(L, (beta,Kb10,Kb20), fixed_params_high_rT, etafunc_args, ETdist) for L in xx]
        fa_low_rT = [OM.fraction_adhered_immobile_crosslinks(L, (beta,Kb10,Kb20), fixed_params_low_rT, etafunc_args, ETdist) for L in xx]
    
    ax = fig.add_subplot(4,2,5)
    ax.semilogx(high_rT_data[:,0], high_rT_data[:,1], 'ko', ms=8)
    ax.semilogx(xx, fa_high_rT, '-k', lw=2)
    ax.set_ylim(0,1.05)
    ax.set_xlim(xx.min(), xx.max())
    ax.set_xlabel('[Alefacept ($\\mu$)]', fontsize='x-large')
    ax.set_ylabel('Fraction cells bound', fontsize='x-large')
    pyplot.xticks(fontsize='x-large')
    pyplot.yticks(fontsize='x-large')
    
    ax = fig.add_subplot(4,2,6)
    ax.semilogx(low_rT_data[:,0], low_rT_data[:,1], 'wo', ms=8, mew=2)
    ax.semilogx(xx, fa_low_rT, '-k', lw=2)
    ax.set_ylim(0,1.05)
    ax.set_xlim(xx.min(), xx.max())
    ax.set_xlabel('[Alefacept ($\\mu$)]', fontsize='x-large')
    ax.set_ylabel('Fraction cells bound', fontsize='x-large')
    pyplot.xticks(fontsize='x-large')
    pyplot.yticks(fontsize='x-large')

    ax = fig.add_subplot(4,2,7)
    ax.plot(high_rT_data[:,3][good_high_data], 
            high_rT_data[:,2][good_high_data], 'ko', ms=8)
    ax.plot(low_rT_data[:,3][good_low_data], 
            low_rT_data[:,2][good_low_data], 'wo', ms=8, mew=2)
    ax.plot([omega*Acell, 20], [0, beta*(20 - omega*Acell)], '-k', lw=2)
    #ax.plot([0, 20], [0, 203*20], '--k')
    ax.set_xlim(0, 20)
    ax.set_ylim(0, 4200)
    pyplot.yticks([0,1000,2000,3000,4000])
    ax.set_ylabel('Avg bridging bonds', fontsize='x-large')
    ax.set_xlabel(r'Avg contact area ($\mu$m$^2$)', fontsize='x-large')
    pyplot.xticks(fontsize='x-large')
    pyplot.yticks(fontsize='x-large')

    fig.subplots_adjust(left=0.10,bottom=0.06,top=0.95,wspace=0.53,hspace=0.33)

    #if figtitle:
    #    fig.text(0.7,0.2, figtitle, fontsize='x-large', 
    #             horizontalalignment='center', verticalalignment='center')
    
    pyplot.show()

def make_plot_deltafunc(params, fixed_params_high_rT,
                        fixed_params_low_rT, high_rT_data, low_rT_data, ET,
                        fignum=None, figtitle=None, xpts=31,
                        model="all_mobile", etafunc=OM.linear_ffunc,
                        area_scaling=1):
    """
    Plot fit results, assuming epitope density is equal on all cells.

    param: Fit parameters, which depend on the model employed.
    fixed_params_high_rT: Fixed parameters for high receptor density data
    fixed_params_low_rT: Fixed parameters for low receptor density data
    high_rT_data: High receptor density data
    low_rT_data: Low receptor density data
    ET: Epitope density
    """
    import matplotlib.pyplot as pyplot

    Acell = fixed_params_high_rT[0]

    # Reproduce plot 1
    fig = pyplot.figure(fignum, figsize=(8,10))
    fig.clear()
    
    xx = 10**numpy.linspace(-4,2,xpts)
    
    if model == "all_mobile":
        beta,Kb10,Kb20,omega = params
        yy_high_rT = numpy.array([M.area_adhered_all_mobile(L, ET, (beta,Kb10,Kb20,omega), fixed_params_high_rT) for L in xx])
        yy_low_rT = numpy.array([M.area_adhered_all_mobile(L, ET, (beta,Kb10,Kb20,omega), fixed_params_low_rT) for L in xx])
    elif model == "fixed_immobile":
        beta,Kb10,Kb20,omega,f = params
        yy_high_rT = numpy.array([M.area_adhered_fixed_immobile(L, ET, f, (beta,Kb10,Kb20,omega), fixed_params_high_rT) for L in xx])
        yy_low_rT = numpy.array([M.area_adhered_fixed_immobile(L, ET, f, (beta,Kb10,Kb20,omega), fixed_params_low_rT) for L in xx])
    elif model == "etafunc":
        beta,Kb10,Kb20,omega = params[:4]
        etafunc_args = params[4:]
        yy_high_rT = [M.area_adhered_immobile_delta_func(L, ET, (beta,Kb10,Kb20,omega), fixed_params_high_rT, etafunc_args, ffunc=etafunc) for L in xx]
        yy_low_rT = [M.area_adhered_immobile_delta_func(L, ET, (beta,Kb10,Kb20,omega), fixed_params_low_rT, etafunc_args, ffunc=etafunc) for L in xx]
    elif model == "xlinks":
        beta,Kb10,Kb20,omega = params[:4]
        etafunc_args = params[4:]
        yy_high_rT = [M.area_adhered_immobile_xlinks_func(L, ET, (beta,Kb10,Kb20,omega), fixed_params_high_rT, etafunc_args, ffunc=etafunc) for L in xx]
        yy_low_rT = [M.area_adhered_immobile_xlinks_func(L, ET, (beta,Kb10,Kb20,omega), fixed_params_low_rT, etafunc_args, ffunc=etafunc) for L in xx]

    good_high_data = high_rT_data[:,1] != 0
    good_low_data = low_rT_data[:,1] != 0

    yy_high_rT = numpy.array(yy_high_rT)
    yy_low_rT = numpy.array(yy_low_rT)

    b_high_rT = (yy_high_rT - omega*Acell)*beta
    b_low_rT = (yy_low_rT - omega*Acell)*beta

    # Apply area scaling factor
    yy_high_rT /= area_scaling
    yy_low_rT /= area_scaling

    ax = fig.add_subplot(4,2,1)
    ax.semilogx(high_rT_data[:,0][good_high_data], high_rT_data[:,3][good_high_data], 'ko', ms=8)
    lims = ax.axis()
    ax.semilogx(xx, yy_high_rT, '-k', lw=2)
    ax.axis(lims)
    ax.axis(xmin=xx.min(), ymin=0, ymax=20, xmax=xx.max())
    ax.set_ylabel('Avg contact area', fontsize='x-large')
    ax.set_xlabel('[Alefacept ($\\mu$)]', fontsize='x-large')
    pyplot.xticks(fontsize='x-large')
    pyplot.yticks(fontsize='x-large')
    ax.set_title(r'$r_T$ = 1200/$\mu m^2$', fontsize='x-large')
    
    ax = fig.add_subplot(4,2,2)
    ax.semilogx(low_rT_data[:,0][good_low_data], low_rT_data[:,3][good_low_data], 'wo', ms=8, mew=2)
    lims = ax.axis()
    ax.semilogx(xx, yy_low_rT, '-k', lw=2)
    ax.axis(lims)
    ax.axis(xmin=xx.min(), ymin=0, ymax=20, xmax=xx.max())
    ax.set_ylabel('Avg contact area', fontsize='x-large')
    ax.set_xlabel('[Alefacept ($\\mu$)]', fontsize='x-large')
    pyplot.xticks(fontsize='x-large')
    pyplot.yticks(fontsize='x-large')
    ax.set_title(r'$r_T$ = 625/$\mu m^2$', fontsize='x-large')

    ax = fig.add_subplot(4,2,3)
    ax.semilogx(high_rT_data[:,0][good_high_data], high_rT_data[:,2][good_high_data], 'ko', ms=8)
    lims = ax.axis()
    ax.semilogx(xx, b_high_rT, '-k', lw=2)
    ax.axis(lims)
    ax.axis(ymin=0, ymax = 4200, xmin=xx.min(), xmax=xx.max())
    ax.set_ylabel('Avg bridging bonds', fontsize='x-large')
    ax.set_xlabel('[Alefacept ($\\mu$)]', fontsize='x-large')
    pyplot.yticks([0,1000,2000,3000,4000])
    pyplot.xticks(fontsize='x-large')
    pyplot.yticks(fontsize='x-large')
    
    ax = fig.add_subplot(4,2,4)
    ax.semilogx(low_rT_data[:,0][good_low_data], low_rT_data[:,2][good_low_data], 'wo', ms=8, mew=2)
    lims = ax.axis()
    ax.semilogx(xx, b_low_rT, '-k', lw=2)
    ax.axis(lims)
    ax.axis(ymin=0, ymax = 4200, xmin=xx.min(), xmax=xx.max())
    ax.set_ylabel('Avg bridging bonds', fontsize='x-large')
    ax.set_xlabel('[Alefacept ($\\mu$)]', fontsize='x-large')
    pyplot.yticks([0,1000,2000,3000,4000])
    pyplot.xticks(fontsize='x-large')
    pyplot.yticks(fontsize='x-large')

    ax = fig.add_subplot(4,2,7)
    ax.plot(high_rT_data[:,3][good_high_data], 
            high_rT_data[:,2][good_high_data], 'ko', ms=8)
    ax.plot(low_rT_data[:,3][good_low_data], 
            low_rT_data[:,2][good_low_data], 'wo', ms=8, mew=2)
    ax.plot([omega*Acell, 20], [0, beta*(20 - omega*Acell)], '-k', lw=2)
    #ax.plot([0, 20], [0, 203*20], '--k')
    ax.set_xlim(0, 20)
    ax.set_ylim(0, 4200)
    pyplot.yticks([0,1000,2000,3000,4000])
    ax.set_ylabel('Avg bridging bonds', fontsize='x-large')
    ax.set_xlabel(r'Avg contact area ($\mu m^2$)', fontsize='x-large')
    pyplot.xticks(fontsize='x-large')
    pyplot.yticks(fontsize='x-large')

    #fig.subplots_adjust(left=0.10,bottom=0.06,top=0.95,wspace=0.33,hspace=0.41,
    #                    right=0.96)
    fig.subplots_adjust(left=0.10,bottom=0.06,top=0.95,wspace=0.53,hspace=0.33)

    #if figtitle:
    #    fig.text(0.7,0.2, figtitle, fontsize='x-large', 
    #             horizontalalignment='center', verticalalignment='center')
    
    pyplot.show()
