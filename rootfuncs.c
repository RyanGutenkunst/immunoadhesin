#include <math.h>
#include <stdio.h>

void area_root_immobile(double inputs[5], double L, double ET, 
        double fit_params[3], double fixed_params[7], double f, double alpha, 
        double G, double outputs[5]){
    double e = inputs[0];
    double r = inputs[1];
    double delta = inputs[2];
    double i = inputs[3];
    double iin = inputs[4];

    double beta = fit_params[0];
    double Kb10 = fit_params[1];
    double Kb20 = fit_params[2];

    double Acell = fixed_params[0];
    double rT = fixed_params[1];
    double KR = fixed_params[2];
    double KE = fixed_params[3];
    double Kx = fixed_params[4];
    double sigmaR = fixed_params[5];
    double sigmaE = fixed_params[6];

    // Assume equality of corresponding immobile and mobile parameters
    double KI = KE;
    double KHI = Kx; 
    double KHE = Kx;
    double Kb01 = Kb10;
    double KFH = Kb20;

    double KG = KR;

    double eT = ET/Acell;

    double ein = sigmaE * e;
    double rin = sigmaR * r;

    double e1 = 2 * KE * L * e;
    double e2 = Kx/2 * e1 * e;
    double e1in = 2 * KE * L * ein;
    double e2in = Kx/2 * e1in * ein;

    double b10 = Kb10 * e1in * rin;
    double b20 = Kb20 * e2in * rin;

    double r1 = KR * L * r;
    double r1in = KR * L * rin;

    double rG = KG * G * r;
    double rGin = KG * G * rin;

    double i1 = 2*KI * L * i;
    double i1in = 2*KI * L * iin;

    double h = KHE * e1 * i;
    double hin = KHE * e1in * iin;

    double b01 = Kb01 * i1in * rin;
    double b11 = KFH * hin * rin;

    outputs[0] = (1-delta)*(e+e1+2*e2+h) + delta*(b10 + 2*b20 + b11 + hin + ein + e1in + 2*e2in) - (1-f)*eT;
    outputs[1] = iin + i1in + b01 + b11 + hin - f*eT;
    outputs[2] = i + i1 + h - f*eT;
    outputs[3] = (r+r1+rG)*(1-alpha*delta) + alpha*delta*(b10 + b20 + b01 + b11 + rin + r1in + rGin) - rT;
    outputs[4] = b10 + b20 + b01 + b11 - beta;
}

void area_root_func(double inputs[3], double L, double ET, double beta, 
        double Kb1, double Kb2, double fixed_params[7], double alpha, 
        double outputs[3]){
    double e = inputs[0];
    double r = inputs[1];
    double delta = inputs[2];

    double Acell = fixed_params[0];
    double rT = fixed_params[1];
    double KR = fixed_params[2];
    double KE = fixed_params[3];
    double Kx = fixed_params[4];
    double sigmaR = fixed_params[5];
    double sigmaE = fixed_params[6];

    double eT = ET/Acell;

    double e1 = 2 * KE * L * e;
    double e2 = KE * L * Kx * e*e;
    double r1 = KR * L * r;
    double b1 = 2 * sigmaR * sigmaE * Kb1 * KE * L * e * r;
    double b2 = sigmaR * sigmaE*sigmaE * Kb2 * KE * L * Kx * e*e * r;

    outputs[0] = eT - (e + e1 + 2*e2 + delta*(b1 + 2*b2 - (1-sigmaE)*(e+e1)\
                - 2*(1-sigmaE*sigmaE) * e2));
    outputs[1] = rT - (r + r1 + alpha*delta*(b1+b2 - (1-sigmaR)*(r + r1)));
    outputs[2] = beta - (b1 + b2);
}

void area_root_immobile_crosslinks(double inputs[5], double L, double ET, double fit_params[3], double fixed_params[7], double ffunc_args[1], double alpha, double outputs[5]){
    double e = inputs[0];
    double r = inputs[1];
    double delta = inputs[2];
    double i = inputs[3];
    double iin = inputs[4];

    double beta = fit_params[0];
    double Kb10 = fit_params[1];
    double Kb20 = fit_params[2];

    double Acell = fixed_params[0];
    double rT = fixed_params[1];
    double KR = fixed_params[2];
    double KE = fixed_params[3];
    double Kx = fixed_params[4];
    double sigmaR = fixed_params[5];
    double sigmaE = fixed_params[6];

    double KI = KE;
    double KHE = Kx;
    double Kb01 = Kb10;
    double KFH = Kb20;

    double eT = ET/Acell;

    double ein = sigmaE * e;
    double rin = sigmaR * r;

    double e1 = 2 * KE * L * e;
    double e2 = Kx/2 * e1 * e;
    double e1in = 2 * KE * L * ein;
    double e2in = Kx/2 * e1in * ein;

    double b10 = Kb10 * e1in * rin;
    double b20 = Kb20 * e2in * rin;

    double r1 = KR * L * r;
    double r1in = KR * L * rin;

    double i1 = 2*KI * L * i;
    double i1in = 2*KI * L * iin;

    double h = KHE * e1 * i;
    double hin = KHE * e1in * iin;

    double b01 = Kb01 * i1in * rin;
    double b11 = KFH * hin * rin;

    double fraction_crosslinked = 2*(delta*(e2in + b20 + hin + b11) + (1-delta)*(e2 + h))/eT;
    double f = 0.13 + ffunc_args[0] * fraction_crosslinked;

    outputs[0] = (1-delta)*(e+e1+2*e2+h) + delta*(b10 + 2*b20 + b11 + hin + ein + e1in + 2*e2in) - (1-f)*eT;
    outputs[1] = iin + i1in + b01 + b11 + hin - f*eT;
    outputs[2] = i + i1 + h - f*eT;
    outputs[3] = (r+r1)*(1-alpha*delta) + alpha*delta*(b10 + b20 + b01 + b11 + rin + r1in) - rT;
    outputs[4] = b10 + b20 + b01 + b11 - beta;
}

void offset_area_root_immobile_xlinks(double inputs[4], double L, double ET, double fit_params[4],
                              double fixed_params[7], double ffunc_args[1], double outputs[4]){
    double e = inputs[0]; 
    double delta = inputs[1];
    double i = inputs[2];
    double iin = inputs[3]; 
    
    double beta = fit_params[0];
    double Kb10 = fit_params[1];
    double Kb20 = fit_params[2];
    double omega = fit_params[3];

    double Acell = fixed_params[0];
    double rT = fixed_params[1];
    double KR = fixed_params[2];
    double KE = fixed_params[3];
    double Kx = fixed_params[4];
    double sigmaR = fixed_params[5];
    double sigmaE = fixed_params[6];

    double eT = ET/Acell;

    double KG = KR;

    double KI = KE;
    double KHI = Kx ;
    double KHE = Kx;
    double Kb01 = Kb10;
    double KFH = Kb20;

    double r = rT/(1+KR*L);

    double ein = sigmaE * e;
    double rin = sigmaR * r;

    double e1 = 2 * KE * L * e;
    double e2 = Kx/2 * e1 * e;
    double e1in = 2 * KE * L * ein;
    double e2in = Kx/2 * e1in * ein;

    double b10 = Kb10 * e1in * rin;
    double b20 = Kb20 * e2in * rin;

    double r1 = KR * L * r;
    double r1in = KR * L * rin;

    double i1 = 2*KI * L * i;
    double i1in = 2*KI * L * iin;

    double h = KHE * e1 * i;
    double hin = KHE * e1in * iin;

    double b01 = Kb01 * i1in * rin;
    double b11 = KFH * hin * rin;

    double fraction_crosslinked = 2*(delta*(e2in + b20 + hin + b11) + (1-delta)*(e2 + h))/eT;
    double f = 0.13 + ffunc_args[0] * fraction_crosslinked;

    outputs[0] = (1-delta)*(e+e1+2*e2+h) + delta*(b10 + 2*b20 + b11 + hin + ein + e1in + 2*e2in) - (1-f)*eT;
    outputs[1] = iin + i1in + b01 + b11 + hin - f*eT;
    outputs[2] = i + i1 + h - f*eT;
    outputs[3] = (b10 + b20 + b01 + b11)*delta/beta + omega - delta;
}

void offset_area_root_fixed_immobile(double inputs[4], double L, double ET, double fit_params[4], 
                             double fixed_params[7], double f, double G, double outputs[4]){
    double e = inputs[0]; 
    double delta = inputs[1];
    double i = inputs[2];
    double iin = inputs[3]; 
    
    double beta = fit_params[0];
    double Kb10 = fit_params[1];
    double Kb20 = fit_params[2];
    double omega = fit_params[3];

    double Acell = fixed_params[0];
    double rT = fixed_params[1];
    double KR = fixed_params[2];
    double KE = fixed_params[3];
    double Kx = fixed_params[4];
    double sigmaR = fixed_params[5];
    double sigmaE = fixed_params[6];

    double eT = ET/Acell;

    double KG = KR;

    double KI = KE;
    double KHI = Kx ;
    double KHE = Kx;
    double Kb01 = Kb10;
    double KFH = Kb20;

    double r = rT/(1+KR*L+KG*G);

    double ein = sigmaE * e;
    double rin = sigmaR * r;

    double e1 = 2 * KE * L * e;
    double e2 = Kx/2 * e1 * e;
    double e1in = 2 * KE * L * ein;
    double e2in = Kx/2 * e1in * ein;

    double b10 = Kb10 * e1in * rin;
    double b20 = Kb20 * e2in * rin;

    double r1 = KR * L * r;
    double r1in = KR * L * rin;

    double i1 = 2*KI * L * i;
    double i1in = 2*KI * L * iin;

    double h = KHE * e1 * i;
    double hin = KHE * e1in * iin;

    double b01 = Kb01 * i1in * rin;
    double b11 = KFH * hin * rin;

    outputs[0] = (1-delta)*(e+e1+2*e2+h) + delta*(b10 + 2*b20 + b11 + hin + ein + e1in + 2*e2in) - (1-f)*eT;
    outputs[1] = iin + i1in + b01 + b11 + hin - f*eT;
    outputs[2] = i + i1 + h - f*eT;
    outputs[3] = (b10 + b20 + b01 + b11)*delta/beta + omega - delta;
}

void offset_area_root_allmobile(double inputs[2], double L, double ET, double fit_params[4], 
                        double fixed_params[7], double G, double outputs[2]){
    double e = inputs[0];
    double delta = inputs[1];
    
    double beta = fit_params[0];
    double Kb10 = fit_params[1];
    double Kb20 = fit_params[2];
    double omega = fit_params[3];

    double Acell = fixed_params[0];
    double rT = fixed_params[1];
    double KR = fixed_params[2];
    double KE = fixed_params[3];
    double Kx = fixed_params[4];
    double sigmaR = fixed_params[5];
    double sigmaE = fixed_params[6];

    double eT = ET/Acell;

    double KG = KR;

    double r = rT/(1+KR*L+KG*G);

    double ein = sigmaE * e;
    double rin = sigmaR * r;

    double e1 = 2 * KE * L * e;
    double e2 = Kx/2 * e1 * e;
    double e1in = 2 * KE * L * ein;
    double e2in = Kx/2 * e1in * ein;

    double b1 = Kb10 * e1in * rin;
    double b2 = Kb20 * e2in * rin;

    double r1 = KR * L * r;
    double r1in = KR * L * rin;

    outputs[0] = (1-delta)*(e+e1+2*e2) + delta*(b1 + 2*b2 + ein + e1in + 2*e2in) - eT;
    outputs[1] = (b1 + b2)*delta/beta + omega - delta;
}

double f_asymptote(double L, double ET, double fit_params[4], double fixed_params[7], double G){
    double beta = fit_params[0];
    double Kb10 = fit_params[1];

    double Acell = fixed_params[0];
    double rT = fixed_params[1];
    double KR = fixed_params[2];
    double KE = fixed_params[3];
    double sigmaR = fixed_params[5];

    double KG = KR;
    double KI = KE;

    double rin = rT*sigmaR/(1.+KR*L+KG*G);
    double Kb01 = Kb10;
    double eT = 1.*ET/Acell;
    return beta*(1.+2*KI*L*(1.+Kb01*rin))/(2.*Kb01*KI*L*eT*rin);
}

void offset_area_root_linear_delta(double inputs[4], double L, double ET, double fit_params[4], double fixed_params[7], double slope, double G, double outputs[4]){
    double delta = inputs[1];

    const double f0 = 0.13;
    double f = f0 + slope*delta;
    f = fmax(fmin(f, 1), f0);

    double f_asy = f_asymptote(L, ET, fit_params, fixed_params, G);
    if(f > f_asy){
        outputs[0] = 0;
        outputs[1] = 0.5;
        outputs[2] = 0;
        outputs[3] = 0;
    }
    else{
        offset_area_root_fixed_immobile(inputs, L, ET, fit_params, fixed_params, f, G, outputs);
    }
}

void area_root_linear_delta(double inputs[5], double L, double ET, double fit_params[3], double fixed_params[7], double slope, double alpha, double G, double outputs[5]){
    double delta = inputs[2];

    const double f0 = 0.13;
    double f = f0 + slope*delta;
    f = fmax(fmin(f, 1), f0);

    double f_asy = f_asymptote(L, ET, fit_params, fixed_params, G);
    if(f > f_asy){
        outputs[0] = 0;
        outputs[1] = 0;
        outputs[2] = 0.5;
        outputs[3] = 0;
        outputs[4] = 0;
    }
    else{
        area_root_immobile(inputs, L, ET, fit_params, fixed_params, f, alpha, G, outputs);
    }
}
