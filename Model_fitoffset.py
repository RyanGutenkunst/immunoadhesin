import numpy
import scipy.optimize

import AlefaceptModel as OldModel
import rootfuncs

delta_max = 0.5

def area_adhered_all_mobile(L, ET, (beta,Kb10,Kb20,omega), fixed_params, 
                            G=0, return_raw=False):
    """
    Area adhered assuming all receptors and epitopes are mobile

    L: Ligand concentration
    ET: Total concentration of epitopes
    beta: Bridging bond density required to balance repulsive force between
          membranes (Eq. 13)
    Kb10: Binding constant of singly-engaged ligand to CD16 (see Fig. 1)
    Kb20: Binding constant of double-engaged ligand to CD16 (see Fig. 1)
    omega: Nospecific adhesion area (Anon in Eqn. 15).
    fixed_params: Sequence containing (Acell, rT, KR, KE, Kx, sigmaR, sigmaE).
                  Here rT is the total density of receptors on the bilayer, while other
                  parameters are as in Table 1.
    G: Nonspecific antibody concentration
    return_raw: If False, return area adhered, bounded by 0 and delta_max
    """
    Acell, rT, KR, KE, Kx, sigmaR, sigmaE = fixed_params

    res = OldModel.area_adhered_all_mobile(L, ET, (beta,Kb10,Kb20), 
                                           fixed_params, return_raw=False)
    if res == 0:
        ic = (0, omega)
    else:
        raw = OldModel.area_adhered_all_mobile(L, ET, (beta,Kb10,Kb20), 
                                               fixed_params, return_raw=True)
        ic = (raw[0], raw[-1]+omega)

    args=(L, ET, (beta,Kb10,Kb20,omega), fixed_params, G)
    (e,delta), infodict,ier,mesg = \
            scipy.optimize.fsolve(rootfuncs.offset_area_root_allmobile, ic, args=args,
                                  full_output=True, fprime = area_jac_allmobile,
                                  maxfev=100000)

    # If the integration failed with those ICs, try the alternate set.
    if ier != 1:
        if res == 0:
            raw = OldModel.area_adhered_all_mobile(L, ET, (beta,Kb10,Kb20), 
                                                   fixed_params,return_raw=True)
            ic = (raw[0], raw[-1]+omega)
        else:
            ic = (0, omega)
        (e,delta), infodict,ier,mesg = \
                scipy.optimize.fsolve(rootfuncs.offset_area_root_allmobile, ic, args=args,
                                      full_output=True, fprime = area_jac_allmobile,
                                      maxfev=100000)
    if ier != 1:
        Lmin,Lmax = OldModel.L_limits(ET, (beta,Kb10,Kb20), fixed_params)
        if abs(L-Lmin) < abs(Lmin - Lmax):
            raw = OldModel.area_adhered_all_mobile(Lmin, ET, (beta,Kb10,Kb20), 
                                                   fixed_params,return_raw=True)
        else:
            raw = OldModel.area_adhered_all_mobile(Lmax, ET, (beta,Kb10,Kb20), 
                                                   fixed_params,return_raw=True)
        ic = (raw[0], 2*omega + max(raw[-1], 0))

        (e,delta), infodict,ier,mesg = \
                scipy.optimize.fsolve(rootfuncs.offset_area_root_allmobile, ic, args=args,
                                      full_output=True, maxfev=100000)

    if ier != 1:
        # Implies that root-finding failed.
        function_args = (L, ET, (beta,Kb10,Kb20,omega), fixed_params, G)
        opt_results = (e,delta), infodict,ier,mesg 
        raise ValueError('Root-finding failed in area_adhered_allmobile.', 
                         (function_args, ic, opt_results))

    if return_raw:
        return e,delta
    else:
        return max(min(delta, delta_max), 0)*Acell

def avg_area_adhered_all_mobile(L, (beta,Kb10,Kb20, omega), fixed_params, 
                                ETdist, eps):
    """
    Average area adhered for a distribution of epitope densities, assuming all receptors
    and epitopes are mobile.

    L: Ligand concentration
    beta: Bridging bond density required to balance repulsive force between
          membranes (Eq. 13)
    Kb10: Binding constant of singly-engaged ligand to CD16 (see Fig. 1)
    Kb20: Binding constant of double-engaged ligand to CD16 (see Fig. 1)
    omega: Nospecific adhesion area (Anon in Eqn. 15).
    fixed_params: Sequence containing (Acell, rT, KR, KE, Kx, sigmaR, sigmaE).
                  Here rT is the total density of receptors on the bilayer, while other
                  parameters are as in Table 1.
    ETdist: Distribution of epitope densities on T Cells
    eps: Precision tolerance for numerical integration.
    """
    if omega == 0:
        return OldModel.avg_area_adhered_all_mobile(L, (beta,Kb10,Kb20), fixed_params, ETdist)
    def integrand(ET, L, (beta,Kb10,Kb20,omega), fixed_params, pdf):
        area = area_adhered_all_mobile(L, ET, (beta,Kb10,Kb20,omega), 
                                       fixed_params)
        weight = pdf.pdf(ET)
        return area*weight

    # Define our integration limits.
    # First, what is minimum epitope count for adhesion, based on omega=0?
    Emin = OldModel.ET_limit(L, (beta,Kb10,Kb20), fixed_params)

    # Then, integrate over the vast majority of the distribution
    Emin = max(Emin, ETdist.ppf(1e-64))
    Emax = ETdist.isf(1e-64)
    if Emax < Emin:
        return 0

    # Normalization factor for integration over distribution
    norm = ETdist.sf(Emin) - ETdist.sf(Emax)

    args = (L, (beta,Kb10,Kb20, omega), fixed_params, ETdist)
    # Do the integration
    int = scipy.integrate.quad(integrand, Emin, Emax, 
                               epsabs=eps, epsrel=eps, args=args,
                               limit=250)
    return int[0]/norm

#
#
#

def area_adhered_fixed_immobile(L, ET, f, (beta,Kb10,Kb20,omega), fixed_params, 
                                G=0, return_raw=False):
    """
    Area adhered assuming a fixed fraction of epitopes are immobile

    L: Ligand concentration
    ET: Total concentration of epitopes
    f: Fraction of immobile epitopes (denoted \eta in the paper)
    beta: Bridging bond density required to balance repulsive force between
          membranes (Eq. 13)
    Kb10: Binding constant of singly-engaged ligand to CD16 (see Fig. 1)
    Kb20: Binding constant of double-engaged ligand to CD16 (see Fig. 1)
    omega: Nospecific adhesion area (Anon in Eqn. 15).
    fixed_params: Sequence containing (Acell, rT, KR, KE, Kx, sigmaR, sigmaE).
                  Here rT is the total density of receptors on the bilayer, while other
                  parameters are as in Table 1.
    G: Nonspecific antibody concentration
    return_raw: If False, return area adhered, bounded by 0 and delta_max
    """
    # First we check whether the immobile fraction is enough to drive adhesion.
    # In that case, delta would go to infinity. 
    Acell,rT,KR,KE,Kx,sigmaR,sigmaE = fixed_params

    fasy = OldModel.f_asymptote(L, ET, (beta,Kb10,Kb20), fixed_params, G=0)
    #if f > fasy:
    #    if return_raw:
    #        return 0,delta_max,0,0
    #    else:
    #        return delta_max * Acell

    try:
        e0f,r0,delta0f,i0,iin0 = OldModel.area_adhered_immobile(L, ET, f, (beta, Kb10, Kb20), fixed_params, G=0, return_raw=True)
        delta0f = max(delta0f, 0)
    except ValueError:
        e0f,delta0f,i0,iin0 = 0,0,0,0

    #e0,delta0 = area_adhered_all_mobile(L, ET, (beta, Kb10, Kb20, omega), fixed_params, G=0, return_raw=True)

    # f = 1 sln
    KI = KE; eT = ET/Acell; Kb01 = Kb10; KG = KR

    rin = sigmaR*rT/(1+KR*L+KG*G)
    i = eT/(1+2*KI*L)
    iin = eT/(1+2*KI*L*(1+Kb01*rin))
    i1in = 2*KI * L * iin
    b01 = i1in*Kb01*rin
    delta = omega/(1-b01/beta)

    # Short circuit root-finding...
    if f == 1:
        if return_raw:
            return (0, delta, i, iin)
        else:
            return max(min(delta,delta_max),0)*Acell

    # My ic for delta is given by the average of 1) delta from f = 1 bounded by
    # (0,delta_max) and 2) delta from omega = 0 + omega
    # Maybe this should be weighted by f?
    ic = (e0f/2, (max(min(delta,delta_max),0) + delta0f+omega)/2, (i0+i)/2,
          (iin + iin0)/2)
    #ic = (e0f*(1-f**2), max(min(delta,delta_max),0)*f**2 + (delta0f+omega)*(1-f**2), i0*(1-f**2)+i*f**2, iin*(1-f**2) + iin0*f**2)

    # If omega is very small relative to delta(omega=0), then let's just use 
    # the delta(omega=0) sln as the ic.
    if delta0f != 0 and abs(omega/delta0f) < 1e-2:
        ic = (e0f, delta0f+omega, i0, iin0)

    if f == 1:
        ic = (0,delta,i,iin)

    args=(L, ET, (beta,Kb10,Kb20,omega), fixed_params, f, G)
    (e,delta,i,iin), infodict,ier,mesg = \
            scipy.optimize.fsolve(rootfuncs.offset_area_root_fixed_immobile, ic, 
                                  args=args, full_output=True, maxfev=int(1e6))
    #print f, mesg
    #print e,delta,i,iin
    #print area_root_fixed_immobile((e,delta,i,iin), *args)

    if ier != 1:
        # Implies that root-finding failed.
        function_args = (L, ET, f, (beta,Kb10,Kb20,omega), fixed_params, G)
        opt_results = (e,delta,i,iin), infodict,ier,mesg 
        raise ValueError('Root-finding failed in area_adhered_fixed_immobile.', 
                         (function_args, ic, opt_results))


    if return_raw:
        return e,delta,i,iin
    else:
        return max(min(delta,delta_max),0)*Acell

def avg_area_adhered_fixed_immobile(L, f, (beta,Kb10,Kb20, omega), fixed_params,
                                    ETdist, eps):
    """
    Average area adhered for a distribution of epitope densities, assuming a
    fixed fraction of epitopes are immobile

    L: Ligand concentration
    f: Fraction of immobile epitopes (denoted \eta in the paper)
    beta: Bridging bond density required to balance repulsive force between
          membranes (Eq. 13)
    Kb10: Binding constant of singly-engaged ligand to CD16 (see Fig. 1)
    Kb20: Binding constant of double-engaged ligand to CD16 (see Fig. 1)
    omega: Nospecific adhesion area (Anon in Eqn. 15).
    fixed_params: Sequence containing (Acell, rT, KR, KE, Kx, sigmaR, sigmaE).
                  Here rT is the total density of receptors on the bilayer, while other
                  parameters are as in Table 1.
    ETdist: Distribution of epitope densities on T Cells
    eps: Precision tolerance for numerical integration.
    """
    def integrand(ET, L, f, (beta,Kb10,Kb20,omega), fixed_params, pdf):
        area = area_adhered_fixed_immobile(L, ET, f, (beta,Kb10,Kb20,omega), 
                                           fixed_params)
        weight = pdf.pdf(ET)
        return area*weight

    # Define our integration limits.
    # First, what is minimum epitope count for adhesion, based on omega=0?
    Emin = OldModel.ET_limit_immobile(L, f, (beta,Kb10,Kb20), fixed_params,
                                      alpha=0)

    # Then, integrate over the vast majority of the distribution
    Emin = max(Emin, ETdist.ppf(1e-64))
    Emax = ETdist.isf(1e-64)

    # Normalization factor for integration over distribution
    norm = ETdist.sf(Emin) - ETdist.sf(Emax)

    args = (L, f, (beta,Kb10,Kb20,omega), fixed_params, ETdist)
    # Do the integration
    int = scipy.integrate.quad(integrand, Emin, Emax, 
                               epsabs=eps, epsrel=eps, args=args)
    return int[0]/norm

#
#
#

def area_root_immobile_ffunc((e,delta,i,iin), L, ET, (beta,Kb10,Kb20,omega), 
                             fixed_params, ffunc_args, ffunc, G):
    """
    Root-finding function for f = ffunc(delta)
    """
    f = ffunc(delta,*ffunc_args)
    fasy = OldModel.f_asymptote(L, ET, (beta,Kb10,Kb20), fixed_params, G)
    if f > fasy:
        return 0,delta_max,0,0
    return rootfuncs.offset_area_root_fixed_immobile((e,delta,i,iin), L, ET, 
                                    (beta,Kb10,Kb20,omega), 
                                    fixed_params, f=f, G=G)


def area_adhered_immobile_delta_func(L, ET, (beta,Kb10,Kb20,omega), 
                                     fixed_params, 
                                     ffunc_args, return_raw=False, 
                                     ffunc = OldModel.linear_ffunc, G = 0):
    """
    Area adhered assuming that the immobile epitope fraction depends on fraction
    of cell that is adhered.

    L: Ligand concentration
    ET: Total concentration of epitopes
    beta: Bridging bond density required to balance repulsive force between
          membranes (Eq. 13)
    Kb10: Binding constant of singly-engaged ligand to CD16 (see Fig. 1)
    Kb20: Binding constant of double-engaged ligand to CD16 (see Fig. 1)
    omega: Nospecific adhesion area (Anon in Eqn. 15).
    fixed_params: Sequence containing (Acell, rT, KR, KE, Kx, sigmaR, sigmaE).
                  Here rT is the total density of receptors on the bilayer, while other
                  parameters are as in Table 1.
    return_raw: If False, return area adhered, bounded by 0 and delta_max
    ffunc_args: Arguments to ffunc
    ffunc: Function specifying relationship between fractional area adhered and
           immobile fraction.
    G: Nonspecific antibody concentration
    """
    Acell,rT,KR,KE,Kx,sigmaR,sigmaE = fixed_params

    fasy = OldModel.f_asymptote(L, ET, (beta,Kb10,Kb20), fixed_params, G=0)
    if ffunc.min(*ffunc_args) > fasy:
        if return_raw:
            return 0,delta_max,0,0
        else:
            return delta_max * Acell

    try:
        e0f,r0,delta0f,i0f,iin0f = OldModel.area_adhered_immobile_ffunc(L, ET, (beta, Kb10, Kb20), fixed_params, ffunc_args, ffunc=ffunc, G=0, return_raw=True)
        fguess = ffunc(delta0f + omega, *ffunc_args)
    except ValueError:
        fguess = 1

    try:
        e0,delta0,i0,iin0 = area_adhered_fixed_immobile(L, ET, fguess, (beta, Kb10, Kb20, omega), fixed_params, G=0, return_raw=True)
    except ValueError:
        # f = 1 sln
        KI = KE; eT = ET/Acell; Kb01 = Kb10; KG = KR

        rin = sigmaR*rT/(1+KR*L+KG*G)
        i0 = eT/(1+2*KI*L)
        iin0 = eT/(1+2*KI*L*(1+Kb01*rin))
        i1in0 = 2*KI * L * iin0
        b01 = i1in0*Kb01*rin
        delta0 = omega/(1-b01/beta)
        e0 = 0

    ic = (e0,delta0,i0,iin0)

    args=(L, ET, (beta,Kb10,Kb20,omega), fixed_params, ffunc_args[0], G)
    (e,delta,i,iin), infodict,ier,mesg = \
            scipy.optimize.fsolve(rootfuncs.offset_area_root_linear_delta, ic, args=args, 
                                  full_output=True)

    if return_raw:
        return e,delta,i,iin

    return Acell*max(min(delta,delta_max),0)

def avg_area_adhered_immobile_delta_func(L, (beta,Kb10,Kb20, omega), 
                                         fixed_params, ETdist, ffunc_args,
                                         ffunc=OldModel.linear_ffunc, eps=1e-3):
    """
    Average area adhered assuming that the immobile epitope fraction depends on fraction
    of cell that is adhered.

    L: Ligand concentration
    beta: Bridging bond density required to balance repulsive force between
          membranes (Eq. 13)
    Kb10: Binding constant of singly-engaged ligand to CD16 (see Fig. 1)
    Kb20: Binding constant of double-engaged ligand to CD16 (see Fig. 1)
    omega: Nospecific adhesion area (Anon in Eqn. 15).
    fixed_params: Sequence containing (Acell, rT, KR, KE, Kx, sigmaR, sigmaE).
                  Here rT is the total density of receptors on the bilayer, while other
                  parameters are as in Table 1.
    ETdist: Distribution of epitope densities on T Cells
    ffunc_args: Arguments to ffunc
    ffunc: Function specifying relationship between fractional area adhered and
           immobile fraction.
    eps: Precision tolerance for numerical integration.
    """
    def integrand(ET, L, (beta,Kb10,Kb20,omega), fixed_params, pdf,
                  ffunc_args, ffunc=OldModel.linear_ffunc):
        area = area_adhered_immobile_delta_func(L, ET, (beta,Kb10,Kb20,omega), 
                                                fixed_params,ffunc_args,
                                                ffunc=ffunc)
        weight = pdf.pdf(ET)
        return area*weight

    Emax = ETdist.isf(1e-64)
    # Define our integration limits.
    # First, what is minimum epitope count for adhesion, based on omega=0?
    Emin = OldModel.ET_limit_immobile_ffunc(L, (beta,Kb10,Kb20), fixed_params, ffunc_args, alpha=0, ffunc=ffunc, G=0, Emax=Emax)

    # Then, integrate over the vast majority of the distribution
    Emin = max(Emin, ETdist.ppf(1e-64))

    # Normalization factor for integration over distribution
    norm = ETdist.sf(Emin) - ETdist.sf(Emax)

    args = (L, (beta,Kb10,Kb20,omega), fixed_params, ETdist, ffunc_args, ffunc)
    # Do the integration
    int = scipy.integrate.quad(integrand, Emin, Emax, 
                               epsabs=eps, epsrel=eps, args=args)
    return int[0]/norm

#
#
#

def ffunc_linear_xlink(fraction_crosslinked, cross_slope, curvature=0):
    f = 0.13 + cross_slope * fraction_crosslinked + 0.5*curvature*fraction_crosslinked**2
    f = numpy.maximum(numpy.minimum(f, 1), 0)
    return f
def ffunc_linear_xlink_min(cross_slope, curvature=0):
    return 0.13
ffunc_linear_xlink.min = ffunc_linear_xlink_min

def area_adhered_immobile_xlinks_func(L, ET, (beta,Kb10,Kb20,omega), 
                                      fixed_params, ffunc_args, 
                                      ffunc = ffunc_linear_xlink,
                                      return_raw=False):
    """
    Area adhered assuming that the immobile epitope fraction depends on fraction
    of cellular epitopes that are crosslinked.

    L: Ligand concentration
    ET: Total concentration of epitopes
    beta: Bridging bond density required to balance repulsive force between
          membranes (Eq. 13)
    Kb10: Binding constant of singly-engaged ligand to CD16 (see Fig. 1)
    Kb20: Binding constant of double-engaged ligand to CD16 (see Fig. 1)
    omega: Nospecific adhesion area (Anon in Eqn. 15).
    fixed_params: Sequence containing (Acell, rT, KR, KE, Kx, sigmaR, sigmaE).
                  Here rT is the total density of receptors on the bilayer, while other
                  parameters are as in Table 1.
    return_raw: If False, return area adhered, bounded by 0 and delta_max
    ffunc_args: Arguments to ffunc
    ffunc: Function specifying relationship between fraction crosslinked and
           immobile fraction.
    G: Nonspecific antibody concentration
    """
    Acell,rT,KR,KE,Kx,sigmaR,sigmaE = fixed_params

    fasy = OldModel.f_asymptote(L, ET, (beta,Kb10,Kb20), fixed_params, G=0)
    if ffunc.min(*ffunc_args) > fasy:
        if return_raw:
            return 0,delta_max,0,0
        else:
            return delta_max * Acell

    KG = KR
    G = 0
    r = rT/(1+KR*L+KG*G)

    # First, solve the model with omega = 0
    e0f,r0,delta0f,i0f,iin0f = OldModel.area_adhered_immobile_crosslinks(L, ET, (beta, Kb10, Kb20), fixed_params, ffunc_args, return_raw=True)
    # What fraction crosslinked did that solution correspond to?
    fcross_res = OldModel.fraction_crosslinked((e0f,r0,delta0f,i0f,iin0f), L, ET, (beta,Kb10,Kb20), fixed_params, alpha=0)
    # What fraction immobile did that solution correspond to?
    fguess = ffunc(fcross_res, *ffunc_args)
    #ic = (e0f,max(delta0f,0)+omega,i0f,iin0f)
    ic = (e0f,delta0f,i0f,iin0f)

    # Now what is the full solution with omega != 0 expected for that eta?
    try:
        e0,delta0,i0,iin0 = area_adhered_fixed_immobile(L, ET, fguess, (beta, Kb10, Kb20, omega), fixed_params, G=0, return_raw=True)
        ic = (e0,delta0,i0,iin0)
    except:
        pass


    args=(L, ET, (beta,Kb10,Kb20,omega), fixed_params, ffunc_args)
    (e,delta,i,iin), infodict,ier,mesg = \
            scipy.optimize.fsolve(rootfuncs.offset_area_root_immobile_xlinks, ic, 
                                  args=args, full_output=True, maxfev=int(1e6))

    if return_raw:
        return e,delta,i,iin

    fcross_result = OldModel.fraction_crosslinked((e,r,delta,i,iin), L, ET, 
                                                  (beta,Kb10,Kb20), 
                                                  fixed_params, alpha=0)
    f_result = ffunc(fcross_result, *ffunc_args)
    f_asym = OldModel.f_asymptote(L, ET, (beta,Kb10,Kb20), fixed_params, 
                                  alpha=0)
    if f_result > f_asym:
        return Acell*delta_max
    else:
        return Acell*max(min(delta,delta_max),0)

def avg_area_adhered_immobile_xlinks_func(L, (beta,Kb10,Kb20, omega), 
                                          fixed_params, ETdist, ffunc_args,
                                          ffunc=ffunc_linear_xlink, eps=1e-3):
    """
    Average area adhered assuming that the immobile epitope fraction depends on fraction
    of cellular epitopes that are crosslinked.

    L: Ligand concentration
    beta: Bridging bond density required to balance repulsive force between
          membranes (Eq. 13)
    Kb10: Binding constant of singly-engaged ligand to CD16 (see Fig. 1)
    Kb20: Binding constant of double-engaged ligand to CD16 (see Fig. 1)
    omega: Nospecific adhesion area (Anon in Eqn. 15).
    fixed_params: Sequence containing (Acell, rT, KR, KE, Kx, sigmaR, sigmaE).
                  Here rT is the total density of receptors on the bilayer, while other
                  parameters are as in Table 1.
    ETdist: Distribution of epitope densities on T Cells
    ffunc_args: Arguments to ffunc
    ffunc: Function specifying relationship between fraction crosslinked and
           immobile fraction.
    eps: Precision tolerance for numerical integration.
    """
    def integrand(ET, L, (beta,Kb10,Kb20,omega), fixed_params, pdf,
                  ffunc_args, ffunc=ffunc_linear_xlink):
        area = area_adhered_immobile_xlinks_func(L, ET, (beta,Kb10,Kb20,omega), 
                                                 fixed_params,ffunc_args,
                                                 ffunc=ffunc)
        weight = pdf.pdf(ET)
        return area*weight

    # Define our integration limits.
    Emax = ETdist.isf(1e-64)

    # First, what is minimum epitope count for adhesion, based on omega=0?
    Emin = OldModel.ET_limit_immobile_crosslinks(L, (beta,Kb10,Kb20), fixed_params, ffunc_args, alpha=0, maxval=Emax)

    # Then, integrate over the vast majority of the distribution
    Emin = max(Emin, ETdist.ppf(1e-64))

    # Normalization factor for integration over distribution
    norm = ETdist.sf(Emin) - ETdist.sf(Emax)

    args = (L, (beta,Kb10,Kb20,omega), fixed_params, ETdist, ffunc_args, ffunc)
    # Do the integration
    int = scipy.integrate.quad(integrand, Emin, Emax, 
                               epsabs=eps, epsrel=eps, args=args)
    return int[0]/norm

def area_jac_allmobile((e,delta), L, ET, (beta,Kb10,Kb20,omega), 
                       fixed_params, G):
    """
    Jacobian function for internal use.
    """
    Acell, rT, KR, KE, Kx, sigmaR, sigmaE = fixed_params
    eT = ET/Acell

    KG = KR

    r = rT/(1+KR*L+KG*G)

    jac = numpy.zeros((2,2))
    jac[0,0] = (1 + 2*KE*L + 4*e*KE*Kx*L)*(1 - delta) + delta*(sigmaE + 2*KE*L*sigmaE + 4*e*KE*Kx*L*sigmaE**2 + 2*Kb10*KE*L*r*sigmaE*sigmaR + 4*e*Kb20*KE*Kx*L*r*sigmaE**2*sigmaR)
    jac[0,1] = (delta*(2*Kb10*KE*L*r*sigmaE*sigmaR + 2*e*Kb20*KE*Kx*L*r*sigmaE**2*sigmaR))/beta
    jac[1,0] = -e - 2*e*KE*L - 2*e**2*KE*Kx*L + e*sigmaE + 2*e*KE*L*sigmaE + 2*e**2*KE*Kx*L*sigmaE**2 + 2*e*Kb10*KE*L*r*sigmaE*sigmaR + 2*e**2*Kb20*KE*Kx*L*r*sigmaE**2*sigmaR
    jac[1,1] = (2*e*Kb10*KE*L*r*sigmaE*sigmaR + 
                e**2*Kb20*KE*Kx*L*r*sigmaE**2*sigmaR)/beta - 1

    return jac
